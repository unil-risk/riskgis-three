<?php
	require_once 'dbConnect.php'; // Connect to the database
		
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	$workspace = $_POST['ws'];
	$matrixID = $_POST['matrixID'];
	$arr = $fields = $weights = array();
	
	$query1 = "SELECT nom AS alternative_name FROM ".$workspace.".matrix_alternatives, ".$workspace.".alternatives 
			WHERE alternatives.id = matrix_alternatives.alt_id AND matrix_alternatives.matrice_id = $matrixID";
			
	$query2 = "SELECT nom AS criteria_name FROM ".$workspace.".matrix_criteria, ".$workspace.".criteria
			WHERE criteria.id = matrix_criteria.critere_id AND matrix_criteria.matrice_id = $matrixID";
	
	$rs1 = pg_query($dbconn,$query1);
	$rs2 = pg_query($dbconn,$query2);
	
	If (!$rs1 || !$rs2) {
		Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
	}
	else {			
		$row = pg_affected_rows($rs1);
		for ($i = 0; $i < $row; $i++) {				
			$fields[$i] = pg_fetch_result($rs1, $i, 0);
		}
		$fields[$i] = 'user_name'; // assign to the last room of the data array
		$fields[$i+1] = 'weight_id';
		
		$row = pg_affected_rows($rs2);
		for ($i = 0; $i < $row; $i++) {				
			$weights[$i] = pg_fetch_result($rs2, $i, 0);
		}
		$weights[$i] = 'user_name'; // assign to the last room of the data array		
		
		Echo '{success:true,data:'.json_encode($fields).',weights:'.json_encode($weights).'}';
	}	
?>