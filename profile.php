<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	session_start();
	if(!isset($_SESSION['usrname'])){
		header("location:../riskgis/index.php");	
	}

	if(isset($_POST['btn-profile-submit']))
	{
		// Connect to the databse		
		if (!$dbconn) {
			echo '{"An error occurred.\n"}';
			exit;
		}
		
		$id = $_SESSION['userid'];
		$workspace = $_SESSION['workspace'];
		$name = $_POST['form-name'];
		$email = $_POST['form-email'];
		$password = md5($_POST['form-pass-word']);
		
		$query= "UPDATE ".$workspace.".users SET display_name = '$name', email = '$email', password = '$password' WHERE id = $id;";
		If (!$rs = pg_query($dbconn,$query)) {
			$msg = pg_last_error($dbconn);
		}
		else {
			$msg = "Your profile information has been successfully updated!"; 
			$_SESSION['displayname'] = $name;
			$_SESSION['email'] = $email;
			$_SESSION['password'] = $_POST['form-pass-word'];
			header("refresh:1;profile.php");
		}
	}
	
?>

<html>
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
	
        <title>RiskGIS - Profile</title>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="shortcut icon" href="./favicon.ico">
		
		<!-- Bootstrap Core CSS -->
		<link href="../riskgis/startbootstrap-grayscale-1.0.6/css/bootstrap.css" rel="stylesheet">
		
		<!-- Custom Fonts -->
		<link href="../riskgis/startbootstrap-grayscale-1.0.6/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
		<link href="http://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	</head>
	
	<body>
	<!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="../riskgis/index.php" target='_blank'>
                    <i class="fa fa-play-circle"></i><span class="light">RISKGIS</span> 						
                </a>	
				<a class="navbar-brand page-scroll" href="profile.php" target='_blank'>
					<span class="light">
						<?php
							echo 'Welcome, ', $_SESSION['displayname'],'!';	
						?>
					</span> 
                </a>						
            </div>
			
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
                <ul class="nav navbar-nav">  					
					<li><a href="" target='_blank'>VIDEO TUTORIAL</a></li>
					<li><a href="" target='_blank'>DOCUMENTATION</a></li>
					<li><a href="../riskgis/logout.php">LOGOUT</a></li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
	
	<!-- Top content -->
    <div class="container" style="padding-top: 60px;">
	  <h1 class="page-header">Edit Profile</h1> 
	  <div class="row">
		<!-- left column -->
		<div class="col-md-4 col-sm-6 col-xs-12">
		  <div class="text-center">
			<img src="icon-user-default.png" class="avatar img-circle img-thumbnail" alt="avatar">
			<h6>Upload a different photo ...</h6>
			<input type="file" class="text-center center-block well well-sm">
		  </div>
		</div>
		
		<!-- edit form column -->
		<div class="col-md-8 col-sm-6 col-xs-12 personal-info">
			<?php if(isset($msg)) { 
				echo '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>'.$msg.'</div>';
				} 
			?>
		  <h3>Personal information</h3>
		  <form class="form-horizontal" role="form" data-toggle="validator" action="" method="post">
			<div class="form-group">
			  <label class="col-lg-3 control-label">User name:</label>
			  <div class="col-lg-8">
				<?php echo '<input class="form-control" readonly= "readonly" value="',$_SESSION['usrname'],'" type="text">'; ?>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-lg-3 control-label">Display name:</label>
			  <div class="col-lg-8">
			    <?php echo '<input class="form-control" name="form-name" value="',$_SESSION['displayname'],'" type="text" required>'; ?>
			  </div>
			</div>			
			<div class="form-group">
			  <label class="col-lg-3 control-label">Email:</label>
			  <div class="col-lg-8">
			    <?php echo '<input class="form-control" name="form-email" value="',$_SESSION['email'],'" type="email" data-error="Bruh, that email address is invalid!" required>'; ?>
			    <div class="help-block with-errors"></div>
			  </div>
			</div>	
			<div class="form-group">
			  <label class="col-md-3 control-label">Password:</label>
			  <div class="col-md-8">
			    <input type="password" data-minlength="6" name="form-pass-word" class="form-password form-control" id="form-pass-word" <?php echo 'value="',$_SESSION['password'],'" required>'; ?>
			 </div>
			</div>
			<div class="form-group">
			  <label class="col-md-3 control-label">Confirm password:</label>
			  <div class="col-md-8">
			     <input type="password" name="form-confirm-pass-word" data-match="#form-pass-word" data-match-error="Whoops, these don't match!" class="form-password form-control" id="form-confirm-pass-word" <?php echo 'value="',$_SESSION['password'],'" required>'; ?>
				 <div class="help-block with-errors"></div>
			  </div>
			</div>
			<div class="form-group">
			  <label class="col-md-3 control-label"></label>
			  <div class="col-md-8">				
				<button type="submit" name="btn-profile-submit" class="btn">Save Changes</button>
				<span></span>
				<input class="btn btn-default" value="Cancel" type="reset">
			  </div>
			</div>
		  </form>
		</div>
	  </div>
	  <hr>
	</div> 
	
	 <!-- Footer -->
    <footer>
        <div class="container text-center">
            <p>Copyright &copy; <a href="http://wp.unil.ch/risk/">Risk Analysis</a> group 2016</p>
        </div>
    </footer>
	
	<!-- Javascript -->		
	<script src="../riskgis/bootstrap-login-form/assets/js/jquery-1.11.1.min.js"></script>
	<script src="../riskgis/bootstrap-login-form/assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="../riskgis/bootstrap-login-form/assets/js/jquery.backstretch.min.js"></script>
	<script src="../riskgis/bootstrap-login-form/assets/js/scripts.js"></script>
	<script src="../riskgis/bootstrap-login-form/assets/js/validator.js"></script>	
	
	</body>

</html>
	