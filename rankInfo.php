<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];
			
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	/* if ($task == 'load') {
		//load the ranking info grid panel 
		$query = "SELECT distinct ranking_results.matrice_id, ranking_results.poids_id, nom, description, users.user_name AS nom_utilisateur FROM ".$workspace.".ranking_results, ".$workspace.".matrix, ".$workspace.".weights, public.users
				WHERE ranking_results.matrice_id = matrix.id AND ranking_results.poids_id = weights.id AND weights.id_utilisateur = users.user_id;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	} */
	
	if ($task == 'load') {
			// allow all ranking info to be visible if logged in with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT distinct ranking_results.matrice_id, ranking_results.poids_id, nom, description, users.user_name AS nom_utilisateur 
				FROM ".$workspace.".ranking_results, ".$workspace.".matrix, ".$workspace.".weights, ".$workspace.".users
				WHERE ranking_results.matrice_id = matrix.id AND ranking_results.poids_id = weights.id AND weights.id_utilisateur = users.id;";
		}
		elseif ($userRole == 'collective') {
			// allow all ranking info (to his/her assigned decision problems) to be visible if logged in with 'collective' role
			$query = "SELECT DISTINCT t2.matrice_id, t2.poids_id, nom, description, users.user_name AS nom_utilisateur 
				FROM
				(SELECT DISTINCT matrice_id, id_utilisateur
				FROM ".$workspace.".weights
				WHERE id_utilisateur = $userID) AS t1, ".$workspace.".ranking_results as t2, ".$workspace.".weights as t3, ".$workspace.".matrix, ".$workspace.".users
				WHERE t1.matrice_id = t2.matrice_id
				AND t2.matrice_id = matrix.id
				AND t3.id = t2.poids_id
				AND t3.id_utilisateur = users.id;";
		}
		else {
			// allow own ranking info if logged in with other roles (i.e. as individually)
			$query = "SELECT distinct ranking_results.matrice_id, ranking_results.poids_id, nom, description, users.user_name AS nom_utilisateur 
				FROM ".$workspace.".ranking_results, ".$workspace.".matrix, ".$workspace.".weights, ".$workspace.".users
				WHERE ranking_results.matrice_id = matrix.id 
				AND ranking_results.poids_id = weights.id 
				AND weights.id_utilisateur = users.id
				AND weights.id_utilisateur = $userID;";
		}
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($_GET['task'] == 'save') {
		$workspace = $_GET['ws'];
		$matrix_id = $_GET['matrixID'];
		$weight_id = $_GET['weightID'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		// delete the existing records first if already existed and replace with the new ones		
		$query = "DELETE FROM ".$workspace.".ranking_results WHERE matrice_id = $matrix_id AND poids_id = $weight_id;";
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the alternative records
			$temp = $phpArray[$i]; 			
			$alt_id = $temp['alternative_id'];
			$distance = $temp['distance'];
			
			$query .= "INSERT INTO ".$workspace.".ranking_results (id, matrice_id, poids_id, alt_id, distance) VALUES (DEFAULT, $matrix_id, $weight_id, $alt_id, $distance);"; 		
		}
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {			
			Echo '{success:true,message:"The ranking results and weights have been saved!"}';
		}
	}
	
	if ($task == 'calculateDyn') {
		// calcute using Compromise Programming method
		$weightCol = $_POST['weightCol'];
		$matrixID = $_POST['matrixID'];
		
		$temp = $_POST['wrecords'];		
		$array = json_decode($temp, true);
		$length = count($array);
		
		// fetch the evaluation records from matrix_values table 
		$query = "CREATE OR REPLACE VIEW ".$workspace.".pivotcolnames AS 
					SELECT matrix_values.alt_id AS alternative_id, alternatives.nom AS alternative_name, 
					matrix_values.critere_id AS criteria_id, criteria.nom AS criteria_name, matrix_values.value
					FROM ".$workspace.".matrix_values, ".$workspace.".alternatives, ".$workspace.".criteria
					WHERE matrix_values.matrice_id = $matrixID AND matrix_values.alt_id = alternatives.id AND matrix_values.critere_id = criteria.id;";

		$query .= "SELECT pivotcode('".$workspace.".pivotcolnames','alternative_id','alternative_name','criteria_id','criteria_name','value','double precision')";	
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				$query = pg_fetch_result($rs, 0, 0); // run to the pivotcode query
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {						
					$matrix = $weight = array(); 
					$row = pg_num_rows($rs); // count the no. of alternatives
					$col = pg_num_fields($rs); // count the no. of criteria
					$fields = $criteria_ids = $wcriteria_ids = array();
					
					for ($i = 0, $j = 2; $i < $row, $j < $col; $i++, $j++) {
						$matrix[] = pg_fetch_row($rs,$i);  // retrieve each rows from the pivot matrix
						$fields[$j] = pg_field_name($rs, $j); // retrieve ID values of each criteria columns
						$temp = explode("_", $fields[$j]);
						$criteria_ids[$j] = end($temp);
						$criteria_ids[$j] = (int)$criteria_ids[$j];
					}
								
					// fetch the weight values from the client side
					for ($i = 0; $i < $length; $i++) {					
						$wcriteria_ids[$i] = $array[$i]['id'];	
						$weight[$i] = $array[$i][$weightCol];				
					}
					
					$sum_weight = array_sum($weight);
					
					foreach ($weight as &$value) {
						$value = $value / $sum_weight; // normalize the weights by dividing with the sum
					}
					
					$max = array(); // array of max values for each criteria
					$min = array(); // array of min values for each criteria
						
					for ($j = 2; $j < $col; $j++){	
						$max[$j] = $min[$j] = $matrix[0][$j];
						for ($i = 0; $i < $row; $i++){				
							if ($max[$j] < $matrix[$i][$j]) $max[$j] = $matrix[$i][$j];
							if ($min[$j] > $matrix[$i][$j]) $min[$j] = $matrix[$i][$j];
						}
					}
					
					for ($i = 0; $i < $row; $i++){
						$sum = 0;
						for ($j = 2, $k=$j-2; $j < $col; $j++, $k++){
							$temp = $matrix[$i][$j];							
							$key = array_search($criteria_ids[$j],$wcriteria_ids); // array search to retrieve the index of $weight array for corresponding criteriaID
							$cell[$i][$j] = pow(($max[$j]-$temp)/($max[$j]-$min[$j]),1)* pow($weight[$key],1); //with parameter p=1	
							$sum = $sum + $cell[$i][$j];
						}			
						$distance[$i]= pow($sum,1); //with parameter p=1
					}
					
					$rs = pg_query($dbconn,$query); //to move back the index
					$i=0; $sdist = $distance; sort($sdist);
					while($obj = pg_fetch_object($rs)){
						$obj->distance = $distance[$i]; $i++;
						for ($j = 0; $j < $row; $j++){
							if ($obj->distance == $sdist[$j]) $obj->ranking = $j+1;
						}			
						$arr[] = $obj;			
					}
					
					Echo '{success:true,rows:'.json_encode($arr).'}';
				}	
				
		}				
	}

	if ($task == 'queryBarDistance') {
		$weightID = $_POST['weightID'];
		$matrixID = $_POST['matrixID'];
		
		//query the distance values for the weightID & matrixID
		$query = "SELECT nom AS alternative_name, distance, rank() over (order by distance asc) as rank  FROM ".$workspace.".ranking_results, ".$workspace.".alternatives
				WHERE ranking_results.alt_id = alternatives.id AND poids_id = $weightID AND matrice_id = $matrixID";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'queryBarWeight') {
		$weightID = $_POST['weightID'];
		$matrixID = $_POST['matrixID'];
		$weightCol = 'w_'.$weightID;
		
		//query the weight values for the weightID & matrixID
		$query = "SELECT nom AS criteria_name, $weightCol  FROM ".$workspace.".criteria, ".$workspace.".matrix_criteria 
				WHERE matrix_criteria.critere_id = criteria.id AND matrix_criteria.matrice_id = $matrixID";
				
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	/* if ($task == 'queryStackedBarDistance') {	
		$matrixID = $_POST['matrixID'];		
		$arr=array();
		
		//query the pivot table for the certain matrixID
		//create the view on 'ranking_results' table to generate User & Alternative column names for a certain matrixID
		$query = "CREATE OR REPLACE VIEW ".$workspace.".pivotcolnames_rank AS 
					SELECT ranking_results.poids_id AS weight_id, users.user_name, ranking_results.alt_id AS alternative_id, nom AS alternative_name, distance
					FROM ".$workspace.".ranking_results, ".$workspace.".alternatives, ".$workspace.".weights, public.users
					WHERE ranking_results.matrice_id = $matrixID AND ranking_results.alt_id = alternatives.id AND ranking_results.poids_id = weights.id AND weights.id_utilisateur = users.user_id
					ORDER BY ranking_results.poids_id, distance;";

		$query .= "SELECT pivotcode_rank('".$workspace.".pivotcolnames_rank','weight_id','user_name','alternative_id','alternative_name','distance','double precision')";	
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				$query = pg_fetch_result($rs, 0, 0);
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {		
					while($obj = pg_fetch_object($rs)){						
						$arr[] = $obj;
					}
					Echo '{success:true,rows:'.json_encode($arr).'}'; 
				}	
		}
	} */
	
	if ($task == 'queryStackedBarDistance') {	
		$matrixID = $_POST['matrixID'];		
		$arr=array();
		
		//query the pivot table for the certain matrixID
		//create the view on 'ranking_results' table to generate User & Alternative column names for a certain matrixID
		$query = "CREATE OR REPLACE VIEW ".$workspace.".pivotcolnames_rank AS 
					SELECT ranking_results.poids_id AS weight_id, users.user_name, ranking_results.alt_id AS alternative_id, nom AS alternative_name, distance
					FROM ".$workspace.".ranking_results, ".$workspace.".alternatives, ".$workspace.".weights, ".$workspace.".users
					WHERE ranking_results.matrice_id = $matrixID AND ranking_results.alt_id = alternatives.id AND ranking_results.poids_id = weights.id AND weights.id_utilisateur = users.id
					ORDER BY ranking_results.poids_id, distance;";

		$query .= "SELECT pivotcode_rank('".$workspace.".pivotcolnames_rank','weight_id','user_name','alternative_id','alternative_name','distance','double precision')";	
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				$query = pg_fetch_result($rs, 0, 0);
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {		
					while($obj = pg_fetch_object($rs)){						
						$arr[] = $obj;
					}
					Echo '{success:true,rows:'.json_encode($arr).'}'; 
				}	
		}
	}
?>
