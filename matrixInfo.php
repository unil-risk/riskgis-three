<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];			
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];

	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') { 
			// allow all matrix sets to be visible if logged in with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT matrix.id, nom, description, id_utilisateur, user_name as nom_utilisateur 
			FROM ".$workspace.".matrix, ".$workspace.".users
			WHERE users.id = id_utilisateur;";
		}
		else {
			// allow only own or assigned matrix sets to be visible if logged in with other roles 
			$query = "(SELECT matrix.*, users.user_name as nom_utilisateur 
			FROM ".$workspace.".matrix, ".$workspace.".users
			WHERE users.id = id_utilisateur
			AND id_utilisateur = $userID) UNION
			(SELECT matrix.*, user_name as nom_utilisateur 
			FROM ".$workspace.".matrix, ".$workspace.".weights, ".$workspace.".users
			WHERE users.id = matrix.id_utilisateur
			AND matrix.id = weights.matrice_id
			AND weights.id_utilisateur = $userID);";
		}	
	
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') { // to create a new decision matrix in the database
	
		// Step1: add a record to the table 'matrix'
		// Step2: add the retrived criteria and alternative ids to the table 'matrix_values' with default values '0'
		// Step3: add the relationship records to the table 'matrix_alternatives' & 'matrix_criteria'
		
		$title = $_POST['matrix_title'];	
		$description = $_POST['matrix_description'];
		$userID = $_POST['userID'];	
		$altArray = explode(',',$_POST['matrix_alternatives']);
		$criteriaArray = explode(',',$_POST['matrix_criteria']);
		
		// Step1: add a record to the table 'matrix'
		$query = "INSERT INTO ".$workspace.".matrix (id, nom, description, id_utilisateur) VALUES (DEFAULT, '$title', '$description', $userID);"; 		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		Else {
			// Step2: add the retrived criteria and alternative ids to the table 'matrix_values' with default values '0'
			// to fetch the last seq no of the added record from 'matrix' table to make it accessible
			$query = "SELECT last_value FROM ".$workspace.".matrix_id_seq";
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					$matrix_id = pg_fetch_result($rs, 0, 0);
					$query = "";
					foreach ($altArray as $alternative){
						foreach ($criteriaArray as $criterion){
							$query .= "INSERT INTO ".$workspace.".matrix_values (id, matrice_id, alt_id, critere_id, value) VALUES (DEFAULT, $matrix_id, $alternative, $criterion, DEFAULT);"; 		
						}
					}
					If (!$rs = pg_query($dbconn,$query)) {
						Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
					}
					else {
					// Step3: add the relationship records to the table 'matrix_alternatives' & 'matrix_criteria'
						$query = "";
						foreach ($altArray as $alternative){
							$query .= "INSERT INTO ".$workspace.".matrix_alternatives (matrice_id, alt_id) VALUES ($matrix_id, $alternative);"; 		
						}
						foreach ($criteriaArray as $criterion){
							$query .= "INSERT INTO ".$workspace.".matrix_criteria (matrice_id, critere_id) VALUES ($matrix_id, $criterion);"; 		
						}
						If (!$rs = pg_query($dbconn,$query)) {
							Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
						}
						else {
							Echo '{success:true,message:"The new evaluation matrix with selected alternatives & criteria have been created!"}';
						}	
					}
				}
		}	
	}
	
	if ($task == 'insert') {
		
		$matrixID = $_POST['matrixID'];
		$altArray = explode(',',$_POST['matrix_alternatives']);
		$criteriaArray = explode(',',$_POST['matrix_criteria']);
		$query = "";
	
		if ($_POST['matrix_criteria'] != ''){
			### if criteria selected
			$query = "SELECT alt_id FROM ".$workspace.".matrix_alternatives WHERE matrice_id = $matrixID;";
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {			
				$row = pg_affected_rows($rs); // retrieve the available alternatives listed in a certain matrixID
				for ($i = 0; $i < $row; $i++) {				
					$alternatives[$i] = pg_fetch_result($rs, $i, 0);
				}
				
				$query = "";
				foreach ($criteriaArray as $criterion) { // loop for each of the newly added columns (criteria)			
					// Step 1: add the retrived criteria and alternative ids to the table 'matrix_values' with default values '0'
					foreach ($alternatives as $alternative){
						$query .= "INSERT INTO ".$workspace.".matrix_values (id, matrice_id, alt_id, critere_id, value) VALUES (DEFAULT, $matrixID, $alternative, $criterion, DEFAULT);";
					}
					// Step2: add the relationship records to the table 'matrix_criteria'
					$query .= "INSERT INTO ".$workspace.".matrix_criteria (matrice_id, critere_id) VALUES ($matrixID, $criterion);"; 		
				}

				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
					exit;
				}
			}
		}
		
		if ($_POST['matrix_alternatives'] != ''){
			### if alternatives selected
			$query = "select critere_id from ".$workspace.".matrix_criteria where matrice_id = $matrixID;";
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {			
				$row = pg_affected_rows($rs); // retrieve the available criteria listed in a certain matrixID
				for ($i = 0; $i < $row; $i++) {				
					$criteria[$i] = pg_fetch_result($rs, $i, 0);
				}
				
				$query = "";
				foreach ($altArray as $alternative) { // loop for each of the newly added alternative records		
					// Step 1: add the retrived criteria and alternative ids to the table 'matrix_values' with default values '0'
					foreach ($criteria as $criterion){
						$query .= "INSERT INTO ".$workspace.".matrix_values (id, matrice_id, alt_id, critere_id, value) VALUES (DEFAULT, $matrixID, $alternative, $criterion, DEFAULT);";
					}
					// Step2: add the relationship records to the table 'matrix_alternatives'
					$query .= "INSERT INTO ".$workspace.".matrix_alternatives (matrice_id, alt_id) VALUES ($matrixID, $alternative);"; 		
				}

				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
					exit;
				}
			}
		}
		
		Echo '{success:true,message:"The newly selected alternatives/criteria have been added. You can start entering the values!"}';	
		
	}	
	
	if ($task == 'edit') { // load the corresponding records from 'matrix_values' table for the selected matrixID
	
		$matrixID = $_POST['matrixID'];
		$arr=array();
		
		// create the view on 'matrix_values' table to generate Alt & Criteria column names for a certain matrixID
		$query = "CREATE OR REPLACE VIEW ".$workspace.".pivotcolnames AS 
					SELECT matrix_values.alt_id AS alternative_id, alternatives.nom AS alternative_name, 
					matrix_values.critere_id AS criteria_id, criteria.nom AS criteria_name, matrix_values.value
					FROM ".$workspace.".matrix_values, ".$workspace.".alternatives, ".$workspace.".criteria
					WHERE matrix_values.matrice_id = $matrixID AND matrix_values.alt_id = alternatives.id AND matrix_values.critere_id = criteria.id;";

		$query .= "SELECT pivotcode('".$workspace.".pivotcolnames','alternative_id','alternative_name','criteria_id','criteria_name','value','double precision')";	

		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				$query = pg_fetch_result($rs, 0, 0);
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					// return the retrived records 'rows' to the grid store in client 
					while($obj = pg_fetch_object($rs)){
						$arr[] = $obj;
					}
					// retrive the column header names, dataIndex & types from the return query 
					$i = pg_num_fields($rs); 
					$fields = $type = array();
					$fields[0]='alternative_id'; $fields[1]='alternative_name';
					
					for ($j = 2; $j < $i; $j++) {
						$fields[$j] = pg_field_name($rs, $j); // get Header & Index value 
						$temp = explode("_", $fields[$j]);
						$criteria_id = end($temp);
						$criteria_id = (int)$criteria_id;
						
						$query = "SELECT type from ".$workspace.".criteria WHERE id = $criteria_id";
						If (!$result = pg_query($dbconn,$query)) {
							Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
						}
						else {
							$type[$j] = pg_fetch_result($result, 0, 0); // get "Type" value of the query criteria
						}
					}
					//	$fieldnames = substr($fieldnames, 0, strlen($fieldnames) - 1)."]";
					Echo '{success:true,data:'.json_encode($fields).',type:'.json_encode($type).',rows:'.json_encode($arr).'}';					
				}
		}
	}
	
	// to save the dirty matrix values to the 'matrix_values' table 
	if ($_GET['task'] == 'save'){
		$workspace = $_GET['ws'];
		$matrix_id = $_GET['matrixID'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) { // loop for each of the dirty records
			$temp = $phpArray[$i]; 			
			$alt_id = $temp['alternative_id'];			
			
			foreach ($temp as $key => $value) {
				
				if ($key != 'alternative_id' && $key != 'alternative_name'){
				//	skip the update query if the key is 'alternative_id' or 'alternative_name'				
					$str = explode("_", $key);
					$criteria_id = end($str);
					$criteria_id =(int)$criteria_id;
					
					$query .= "UPDATE ".$workspace.".matrix_values SET value = '$value' WHERE matrice_id = $matrix_id AND alt_id = $alt_id AND critere_id = $criteria_id;";			
				}
			}						
		}
		
		If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
		else {			
			Echo '{success:true,message:"The matrix values have been updated!"}';
		}	
	}
	
	if ($task == 'removeAlt') { // happens when an alternative is being removed from the matrix
	
		// Step1: remove the retrived alternative id from the table 'matrix_values' 
		// Step2: remove the relationship record from the table 'matrix_alternatives' 		
		$matrix_id = $_POST['matrixID'];
		$alt_id = $_POST['altID'];
		
		$query = "DELETE FROM ".$workspace.".matrix_values WHERE matrice_id = $matrix_id AND alt_id = $alt_id;";
		$query .= "DELETE FROM ".$workspace.".matrix_alternatives WHERE matrice_id = $matrix_id AND alt_id = $alt_id;";
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {			
			Echo '{success:true,message:"The selected alternative has been removed!"}';
		}
	}

	if ($task == 'removeCriteria') { // happens when a criterion is being removed from the matrix
	
		// Step1: remove the retrived criteria id from the table 'matrix_values' 
		// Step2: remove the relationship record from the table 'matrix_criteria' 
		$matrix_id = $_POST['matrixID'];
		$criteria_id = $_POST['criteriaID'];
		
		$query = "DELETE FROM ".$workspace.".matrix_values WHERE matrice_id = $matrix_id AND critere_id = $criteria_id;";
		$query .= "DELETE FROM ".$workspace.".matrix_criteria WHERE matrice_id = $matrix_id AND critere_id = $criteria_id;";
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {			
			Echo '{success:true,message:"The selected criteria has been removed!"}';
		}
	}
	
	if ($task == 'editInfo') {
		$ID = $_POST['ID'];	
		$title = $_POST['matrix_title'];
		$desc = $_POST['matrix_description'];
		
		$query= "UPDATE ".$workspace.".matrix SET nom = '$title' , description = '$desc' WHERE id = $ID;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The decision problem information has been updated!"}';
		}
	}
	
	if ($task == 'delete') { // to delete the selected matrix sets from the database and remove the respective weight_cols from 'criteria' table
		$temp = $_POST['IDs'];		
		$array = json_decode($temp, true);
		$length = count($array);
		
		// Step 1: query the weight sets for a certain matrixID
		for ($i = 0; $i < $length; $i++) {
			$ID = $array[$i]['id'];
		
			$query = "SELECT indice FROM ".$workspace.".weights WHERE matrice_id = $ID;";
			if (!$rs = pg_query($dbconn,$query)){
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				exit;
			}
			else {
			//	if success, retrieve the weight_cols and drop the respective columns
				$query = "";
				$row = pg_affected_rows($rs); 
				if ($row > 0){
					for ($j = 0; $j < $row; $j++) {				
						$mapping_index[$j] = pg_fetch_result($rs, $j, 0);
						$query .= "ALTER TABLE ".$workspace.".criteria DROP COLUMN $mapping_index[$j] RESTRICT;";
					}
					if (!$rs = pg_query($dbconn,$query)){
						Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';	
						exit;
					}
				}				
			}
		}
		
		// Step 2: delete the selected matrix sets
		$query = "";
		for ($i = 0; $i < $length; $i++) {
			$ID = $array[$i]['id'];
			$query .= "DELETE FROM ".$workspace.".matrix WHERE id = $ID;";				
		}			
		if (!$rs = pg_query($dbconn,$query)){
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}	
		else {
			Echo '{success:true, message: "The selected decision problems have been deleted!"}';
		}
	}
?>