<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];			
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	// to load the available weight sets from the database
	/* if ($task == 'load') {
		$query = "SELECT weights.id, weights.matrice_id, nom, description, user_name AS nom_utilisateur, delai, remarques, indice, email_add 
		FROM ".$workspace.".weights, ".$workspace.".matrix, public.users WHERE weights.matrice_id = matrix.id AND public.users.user_id = weights.id_utilisateur;";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	} */
	
	if ($task == 'load') {
		// allow all weight sets to be visible if logged in with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT weights.id, weights.matrice_id, weights.id_utilisateur, nom, description, user_name AS nom_utilisateur, delai, remarques, indice, email 
			FROM ".$workspace.".weights, ".$workspace.".matrix, ".$workspace.".users 
			WHERE weights.matrice_id = matrix.id 
			AND users.id = weights.id_utilisateur;";
		}
		elseif ($userRole == 'collective') {
			// allow all weight sets (to his/her assigned decision problems) to be visible if logged in with 'collective' role
			$query = "SELECT t2.id, t2.matrice_id, t2.id_utilisateur, nom, description, user_name AS nom_utilisateur, delai, remarques, indice, email 
			FROM 
			(SELECT DISTINCT matrice_id 
			FROM ".$workspace.".weights
			WHERE id_utilisateur = $userID) AS t1, ".$workspace.".weights as t2, ".$workspace.".matrix, ".$workspace.".users 
			WHERE t1.matrice_id = t2.matrice_id
			AND t2.matrice_id = matrix.id 
			AND users.id = t2.id_utilisateur;";
		}
		else {
			// allow only own weight sets to be visible if logged in with other roles (i.e. as individually)
			$query = "SELECT weights.id, weights.matrice_id, weights.id_utilisateur, nom, description, user_name AS nom_utilisateur, delai, remarques, indice, email
			FROM ".$workspace.".weights, ".$workspace.".matrix, ".$workspace.".users
			WHERE weights.matrice_id = matrix.id 
			AND weights.id_utilisateur = $userID
			AND users.id = weights.id_utilisateur;";
		}
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	// to add new weight set record and append an empty weight column to the 'criteria' table
	if ($task == 'add') {
		// retrieve POST data submitted by form
		$user_id = $_POST['user_id'];
		$matrix_id = $_POST['matrix_id'];
		$deadline = $_POST['enddt'];
		$remarks = $_POST['remarks'];
				
		if ($deadline) {
			$query = "INSERT INTO ".$workspace.".weights (id, id_utilisateur, remarques, matrice_id, delai) VALUES (DEFAULT, $user_id, '$remarks', $matrix_id,'$deadline');"; 		
		}
		else {
			$query = "INSERT INTO ".$workspace.".weights (id, id_utilisateur, remarques, matrice_id) VALUES (DEFAULT, $user_id, '$remarks', $matrix_id);"; 		
		}
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else
		{	// to fetch the last seq no and add a new empty weight column
			$query = "SELECT last_value FROM ".$workspace.".weights_id_seq";
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
			}
			else {
				$last_value = pg_fetch_result($rs, 0, 0);
				$mapping_index = 'w_'. $last_value;
				$query = "UPDATE ".$workspace.".weights SET indice = '$mapping_index' WHERE id = $last_value;"; 
				$query .= "ALTER TABLE ".$workspace.".criteria ADD COLUMN $mapping_index double precision NOT NULL DEFAULT 0;";
				If (!$rs = pg_query($dbconn,$query)) {
					Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				}
				else {
					Echo '{success:true,message:"The new empty weight set has been created!"}';
				}
			}
		}
	}
	
	// to load the weight values from the 'criteria' table for corresponding weight column and matrixID
	if ($task == 'edit') {
		$weightCol = $_POST['weightCol'];
		$matrixID = $_POST['matrixID'];
		
		$query = "SELECT criteria.id, criteria.nom, criteria.description, $weightCol FROM ".$workspace.".criteria, ".$workspace.".matrix_criteria 
				WHERE matrix_criteria.critere_id = criteria.id AND matrix_criteria.matrice_id = $matrixID";
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}	
	
	// to save the weight values to the 'criteria' table for corresponding weight column and matrixID
	if ($_GET['task'] == 'saveweight'){
		$workspace = $_GET['ws'];
		$weightCol = $_GET['weightCol'];
		$postdata = file_get_contents("php://input"); 
		$phpArray = json_decode($postdata, true);
		$length = count($phpArray);	
		
		for ($i = 0; $i < $length; $i++) {
			$temp = $phpArray[$i];
			$id = $temp['id'];
			$wCol = $temp[$weightCol];
		
			$query= "UPDATE ".$workspace.".criteria SET $weightCol = '$wCol' WHERE id = $id;";
			If (!$rs = pg_query($dbconn,$query)) {
				Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
				exit;
			}
		}
		// arrives here if all records are successfully updated
		Echo '{success:true,message:"The weight values have been updated!"}';
	}
	
	// to edit the weight set information 
	if ($task == 'editInfo') {
		$ID = $_POST['ID'];	
		$id = $_POST['user_id'];
		$deadline = $_POST['enddt'];
		$remarks = $_POST['remarks'];	
		
		if ($deadline) {
			$query= "UPDATE ".$workspace.".weights SET id_utilisateur = $id , delai = '$deadline', remarques = '$remarks' WHERE id = $ID;";
		}
		else {
			$query= "UPDATE ".$workspace.".weights SET id_utilisateur = $id , remarques = '$remarks' WHERE id = $ID;";	
		}
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The weight information has been successfully updated!"}';
		}
	}
	
	if ($task == 'delete') { // to delete the selected weight sets from the 'weights' table and remove the respective weight_cols from 'criteria' table
		$temp = $_POST['IDs'];		
		$array = json_decode($temp, true);
		$length = count($array);
		
		for ($i = 0; $i < $length; $i++) {
			$ID = $array[$i]['id'];
			$query .= "DELETE FROM ".$workspace.".weights WHERE id = $ID;";
			$query .= "ALTER TABLE ".$workspace.".criteria DROP COLUMN w_$ID RESTRICT;";
		}			
		if (!$rs = pg_query($dbconn,$query)){
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}	
		else {
			Echo '{success:true,message: "The selected weight sets have been deleted!"}';	
		}
	}
?>