<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];	
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];
		
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	if ($task == 'load') {
			// allow all criteria to be visible if logged in with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT criteria.id, nom, description, categorie, type, id_utilisateur, user_name AS nom_utilisateur
			FROM ".$workspace.".criteria, ".$workspace.".users
			WHERE users.id = id_utilisateur;";
		}
		elseif ($userRole == 'collective') {
			// allow only related criteria (to his/her assigned decision problems) to be visible if logged in with 'collective' role
			$query = "SELECT DISTINCT criteria.id, nom, description, categorie, type, id_utilisateur, user_name AS nom_utilisateur
			FROM 
			(SELECT DISTINCT matrice_id
			FROM ".$workspace.".weights
			WHERE id_utilisateur = $userID) AS t1, ".$workspace.".matrix_criteria as t2, ".$workspace.".criteria, ".$workspace.".users
			WHERE t1.matrice_id = t2.matrice_id
			AND t2.critere_id = criteria.id
			AND users.id = id_utilisateur;";
		}
		else {
			// allow only own criteria to be visible if logged in with other roles (i.e. as individually)
			$query = "SELECT criteria.id, nom, description, categorie, type, id_utilisateur, user_name AS nom_utilisateur
			FROM ".$workspace.".criteria, ".$workspace.".users
			WHERE id_utilisateur = $userID
			AND users.id = id_utilisateur;";
		}
		
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
	
	if ($task == 'add') {
		// retrieve POST data submitted by form
		$criteria_name = $_POST['criteria_name'];
		$criteria_description = $_POST['criteria_description'];
		$category = $_POST['category'];		
		$type = $_POST['type'];		
		$userID = $_POST['userID'];
		
		$query = "INSERT INTO ".$workspace.".criteria VALUES (DEFAULT, '$criteria_name', '$criteria_description', '$category', '$type', '$userID');"; 
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else
		{
			Echo '{success:true,message:"The new criteria has been added!"}';
		}
	}
	
	if ($task == 'edit') {
		
		$ID = $_POST['ID'];		
		$nom = $_POST['name'];
		$desc = $_POST['description'];
		$category = $_POST['category'];
		$type = $_POST['type'];
		
		// query to update the record in the table
		$query = "UPDATE ".$workspace.".criteria SET nom = '$nom', description = '$desc', categorie = '$category', type = '$type' WHERE id = $ID;";
		$arr = array();
				
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
			Echo '{success:true,message:"The criterion information has been successfully updated!"}';		
		}
			
	}
	
	if ($task == 'delete') { // to delete the selected criteria from the table
		$temp = $_POST['IDs'];		
		$array = json_decode($temp, true);
		$length = count($array);
		
		for ($i = 0; $i < $length; $i++) {
			$ID = $array[$i]['id'];
			$query .= "DELETE FROM ".$workspace.".criteria WHERE id = $ID;";						
		}			
		if (!$rs = pg_query($dbconn,$query)){
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}	
		else {
			Echo '{success:true,message: "The selected criteria have been deleted!"}';	
		}
	}
?>