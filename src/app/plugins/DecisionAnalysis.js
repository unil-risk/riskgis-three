/**
 * Copyright (c) 2016 
 * 
 * Published under the GPL license.
 */
 
/**
 * @require plugins/Tool.js
 */
 
 /** api: (define)
 *  module = riskgis.plugins
 *  class = DecisionAnalysis
 */
 
 /** api: (extends)
 *  plugins/Tool.js
 */

Ext.ns("riskgis.plugins");

/** api: constructor
 *  .. class:: DecisionAnalysis(config)
 *
 *    Plugin for the decision analysis module: to define decision problem, criteria, weights and visualize group ranking information.
 *    
 */
 
 riskgis.plugins.DecisionAnalysis = Ext.extend(gxp.plugins.Tool, {

	/** api: ptype = riskgis_DecisionAnalysis */
	ptype: "riskgis_DecisionAnalysis",
	
	/** api: config[DecisionAnalysisMenuText]
     *  ``String``
     *  Text for the menu item (i18n).
     */
    DecisionAnalysisMenuText: "Decision Analysis",

    /** api: config[DecisionAnalysisActionTip]
     *  ``String``
     *  Text for the tooltip (i18n).
     */
    DecisionAnalysisActionTip: "Tool to define decision problem, criteria, weights and visualize group ranking information",
	
	addActions: function() {
		var actions = riskgis.plugins.DecisionAnalysis.superclass.addActions.apply(this, [			 						
		{
			menuText: this.DecisionAnalysisMenuText,
            iconCls: "riskgis-icon-decision-analysis",
            tooltip: this.DecisionAnalysisActionTip,
			scope: this,
            handler: function(args) {
				if (args == 'create-eva-matrix') this.createEvaMatrix();
				else if (args == 'view-eva-matrix') this.listEvaMatrices();
				else if (args == 'create-criteria') this.createCriterion();
				else if (args == 'view-criteria') this.listCriteria();
				else if (args == 'create-weight') this.createWeight();
				else if (args == 'list-weight') this.listWeight();				
				else this.listGroupRank();					
			}			
		}
		]);
	},

	/** api: method[createEvaMatrix]
     */
	createEvaMatrix: function() {
		
		var matrixAddForm = new Ext.form.FormPanel({
			labelWidth: 75, // label settings here cascade unless overridden
			url:'matrixInfo.php',
			frame:true,			
			bodyStyle:'padding:5px 5px 0',
			width: 450,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Decision Problem Information',
				collapsible: true,
				defaultType: 'textfield',
				defaults: {
					anchor: "90%",
					allowBlank: false,
					msgTarget: "side"
				},
				collapsed: false,
				items :[{
						fieldLabel: 'Title',
						name: 'matrix_title',
						emptyText: 'Name of the decision problem ...'
					},{
						fieldLabel: 'Description',
						xtype: 'textarea',
						name: 'matrix_description',
						emptyText: 'Description of the decision problem ...',
						allowBlank: true
					},{
						fieldLabel: 'Alternatives',
						name: 'matrix_alternatives',
						hiddenName: 'matrix_alternatives',
						xtype: 'lovcombo',						
						mode: "local",
						editable: false,	
						hideOnSelect:false,
						store: new Ext.data.JsonStore({ 
							url: 'altInfo.php',
							baseParams: {
								task: 'loadGrid',
								ws: 'riskgis_three',
								userID: userid,
								userRole: role
							},
							autoLoad: true,
							root: 'rows',
							fields : ['id','nom']
						}),							
						valueField: 'id',
						displayField: 'nom',
						emptyText: 'select the alternatives ...',
						triggerAction: 'all'						  
					},{
						fieldLabel: 'Criteria',
						name: 'matrix_criteria',
						hiddenName: 'matrix_criteria',
						xtype: 'lovcombo',							
						mode: "local",
						editable: false,
						hideOnSelect:false,
						store: new Ext.data.JsonStore({ 
							url: 'criteriaInfo.php',
							baseParams: {
								task: 'load',
								ws: 'riskgis_three',
								userID: userid,
								userRole: role
							},
							autoLoad: true,
							root: 'rows',
							fields : ['id','nom']
						}),							
						valueField: 'id',
						displayField: 'nom',
						emptyText: 'select the criteria ...',
						triggerAction: 'all'						  
					}]
			}],
			buttons: [{
				text: 'Create',
				icon: 'src/gxp/theme/img/silk/add.png',
				handler: function(){
					//add a new evaluation matrix record to the table
					var form = matrixAddForm.getForm();				
					if (form.isValid()) {
						form.submit({
							url: 'matrixInfo.php',
							params: {
								task: 'add',
								ws: 'riskgis_three',
								userID: userid
							},
							waitMsg : 'Please wait...',
							success: function(form, action) {						
								Ext.Msg.alert('Success', action.result.message);	
								form.reset();								
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}	
						});	
					}
					else {
						Ext.Msg.alert('Validation Error', 'The form is not valid!' );
					}
				}
			},{
				text: 'Reset',
				icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
				handler: function(){
					// reset the form and close the dialog box 
					matrixAddForm.getForm().reset();
				}
			}]	
		});
	
		var matrixAddWin = new Ext.Window({
			title: 'Define a new decision problem ..',
			layout: 'form',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: false,
			closeable: true,
			buttonAlign:'right',
			items: [matrixAddForm],
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: 'The decision-making process can benefit from using <b>Multi-Criteria Evaluation (MCE) </b>methods. These methods consider different alternatives of a problem with the aim of addressing trade-offs between alternatives with inclusion of additional important criteria than the traditional cost-benefit analysis. To compare between different management alternatives, the effect of each alternative should be evaluated against each criterion.' + '<br><br>' + 'In this platform, Compromise Programming (CP) method (<a href="./Simonovic_2010.pdf" target="_blank">Simonovic</a>, 2010) is used to calculate the ranking of alternative options. This method identifies alternatives which are the closest to the ideal solution by means of distance (measures of closeness). It supports the selection of an optimum solution assuming that decision makers seek a solution which is as close as possible to the ideal one.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});						
						}
				}]
		});
		
		matrixAddWin.show();
	},

	/** api: method[editEvaMatrix]
     */
	editEvaMatrix: function(id, rec, grid) {
		var combox = new Ext.form.ComboBox({			
			store: new Ext.data.ArrayStore({
				fields: ["value", "text"],
				data: [
					[ 1, "Extremely Bad" ],
					[ 2, "Very Bad" ],
					[ 3, "Bad" ],
					[ 4, "More or Less Bad" ],
					[ 5, "Moderate" ],
					[ 6, "More or Less Good" ],
					[ 7, "Good" ],
					[ 8, "Very Good" ],
					[ 9, "Perfect" ]
				]
			}),
			valueField: "value",
			displayField: "text",
			mode: "local",
			triggerAction: 'all',
			allowBlank: false
		});
		
		Ext.Ajax.request({
			url : 'matrixInfo.php',
			params : {
				task: 'edit',
				ws: 'riskgis_three',
				matrixID: id
			},
			success : function(r){
				//create a json object from the response string
				var res = Ext.decode(r.responseText, true);
				// if we have a valid json object, then process it
				if(res !== null &&  typeof (res) !==  'undefined'){
					//create extjs json store 
					var evaMatrixStore = new Ext.data.JsonStore({
						url: 'matrixInfo.php',
						root: 'rows',
						fields: res.data,
						autoLoad: true,
						baseParams: {
							task: 'edit',
							ws: 'riskgis_three',
							matrixID: id
						}
					});												
					//	Retrive the dynamic column names from the php response
					var cols = [];
					cols[0] = {header: "ID", dataIndex: "alternative_id", width:5, hidden: true}; // alternative_id
					cols[1] = {header: "Alternatives Vs Criteria", dataIndex: "alternative_name", width:150}; // alternative_name
					for (var i = 2; i < res.data.length; i++)
					{
						cols[i] = {header: res.data[i], dataIndex: res.data[i], 
						editor: res.type[i] == 'Qualitative'? combox: new Ext.form.NumberField(),
						listeners: {
							'contextmenu': function(column,evaMatrixGrid,rowIndex,event){
								// show the right-click menu 
								event.stopEvent();
								var record = evaMatrixGrid.getStore().getAt(rowIndex);
								var menu = new Ext.menu.Menu({
									items: [{
										text: 'Delete Column',
										handler: function() {
											// if yes, remove the criteria column from the column model and delete it from the tables
											Ext.Msg.show({
												title:'Delete Column?',
												msg: 'Are you sure to delete this criteria column: "'+ column.header + '" from the matrix?',
												buttons: Ext.Msg.YESNOCANCEL,
												fn: function(buttonId, text, opt){
													if (buttonId == 'yes'){
														// remove the column from the column model and reconfigure
														var cm = evaMatrixGrid.colModel;
														var curColConfig = cm.config;
																					
														colIndex = cm.findColumnIndex(column.dataIndex);																					
														if (colIndex >= 0){
															curColConfig.splice(colIndex, 1);
														}
																					
														cm.setConfig(curColConfig, true); // set true to keep the orignial editors
														evaMatrixGrid.reconfigure(evaMatrixGrid.getStore(), new Ext.grid.ColumnModel(curColConfig));
														
														//	Ajax request to delete the column
														var str = column.header;
														var str_res = str.split("_");
														Ext.Ajax.request({
															url:'matrixInfo.php',
															params: {
																task: 'removeCriteria',
																ws: 'riskgis_three',
																matrixID: id,
																criteriaID: str_res[str_res.length-1] //criteriaID
															},																						
														success: function(response, opts) {				
															var obj = Ext.decode(response.responseText);										
															if (obj.success = true) Ext.Msg.alert('Success', obj.message);						
															else Ext.Msg.alert('Failed', obj.message);						
															}
														});
													}
												},
												animEl: 'elId',
												icon: Ext.MessageBox.QUESTION
											});
										}																		
									},{
										text: 'Delete Row',
										handler: function() {
											// if yes, remove the alternative row from the store and delete it from the tables
											Ext.Msg.show({
												title:'Delete Row?',
												msg: 'Are you sure to delete this alternative row: "'+ record.get('alternative_name') + '" from the matrix?',
												buttons: Ext.Msg.YESNOCANCEL,
												fn: function(buttonId, text, opt){
													if (buttonId == 'yes'){
														// remove the record from the store
														evaMatrixGrid.getStore().remove(record);
														// Ajax request to delete the row
														Ext.Ajax.request({
															url:'matrixInfo.php',
															params: {
																task: 'removeAlt',
																ws: 'riskgis_three',
																matrixID: id,
																altID: record.get('alternative_id')
															},																						
															success: function(response, opts) {				
																var obj = Ext.decode(response.responseText);
																if (obj.success = true) Ext.Msg.alert('Success', obj.message);						
																else Ext.Msg.alert('Failed', obj.message);	
															}
														});
													}
												},
												animEl: 'elId',
												icon: Ext.MessageBox.QUESTION
											});
										}
									}]
								}).showAt(event.xy);
							}
																
						}											
					
						};												
					}
												
					var matrixColModel = new Ext.grid.ColumnModel({
						defaults: {
							sortable: true,
							width: 100
						},
						columns: cols														
					});
																					
					//Grid for impact matrix 
					var evaMatrixGrid = new Ext.grid.EditorGridPanel({
						title: 'Evaluation of Alternatives Against Critreia',
						store: evaMatrixStore,
						colModel: matrixColModel,
						viewConfig: {														
							forceFit: true									
						},
						sm: new Ext.grid.RowSelectionModel({singleSelect:true}),												
						frame: true,
						stripeRows : true,
						region: 'center',
						split: true,
						buttons: [{
							text: 'Add', // to add new alternative/criteria
							iconCls: 'add',
							handler: function(){
								// show a form dialbog box to select the alternatives/criteria
								var matrixUpdateForm = new Ext.form.FormPanel({
									labelWidth: 75, // label settings here cascade unless overridden
									frame:true,			
									bodyStyle:'padding:5px 5px 0',
									width: 300,	
									autoHeight:true,
									items: [{
										fieldLabel: 'Alternatives',
										name: 'matrix_alternatives',
										hiddenName: 'matrix_alternatives',
										xtype: 'lovcombo',																	
										mode: "local",
										editable: false,					
										hideOnSelect:false,
										store: new Ext.data.JsonStore({ 
											url: 'altInfo.php',
											baseParams: {
												task: 'loadGrid',
												ws: 'riskgis_three',
												userID: userid,
												userRole: role
											},
											autoLoad: true,
											root: 'rows',
											fields : ['id','nom']
										}),							
										valueField: 'id',
										displayField: 'nom',	
										emptyText: 'select the alternatives ...',
										triggerAction: 'all'
									},{
										fieldLabel: 'Criteria',
										name: 'matrix_criteria',
										hiddenName: 'matrix_criteria',
										xtype: 'lovcombo',							
										mode: "local",
										editable: false,
										hideOnSelect:false,
										store: new Ext.data.JsonStore({ 
											url: 'criteriaInfo.php',
											baseParams: {
												task: 'load',
												ws: 'riskgis_three',
												userID: userid,
												userRole: role
											},
											autoLoad: true,
											root: 'rows',
											fields : ['id','nom', 'type']
										}),							
										valueField: 'id',
										displayField: 'nom',
										emptyText: 'select the criteria ...',
										triggerAction: 'all'						  
									}],	
									buttons: [{
										text: 'Add',
										iconCls: 'add',
										handler: function() {
											var form = matrixUpdateForm.getForm();				
											var selectedCriteriaIdArray = form.findField("matrix_criteria").getValue().split(','); 
												
											if (form.findField("matrix_criteria").getValue() != '' || form.findField("matrix_alternatives").getValue() != '') {												
												form.submit({
													url: 'matrixInfo.php',
													params: {
														task: 'insert',
														ws: 'riskgis_three',
														matrixID: id
													},
													waitMsg : 'Please wait...',
													success: function(form, action) {	
														if (action.result.success == true) {
															Ext.Msg.alert('Success', action.result.message);
															evaMatrixStore.reload(); // reload for the new alternatives
															form.reset();
															// configure a new column model with new columns added
															var cm = evaMatrixGrid.colModel;
															var curColConfig = cm.config;
															
															for(var i =0; i < selectedCriteriaIdArray.length; i++){
																var index = form.findField("matrix_criteria").getStore().findExact('id', selectedCriteriaIdArray[i]);
																var record = form.findField("matrix_criteria").getStore().getAt(index);
																var type = record.get('type');
																var name = record.get('nom');
																						
																curColConfig.push({
																	header: name + '_' + selectedCriteriaIdArray[i],
																	width: 100,
																	dataIndex: name + '_' + selectedCriteriaIdArray[i],
																	editor: type == 'Qualitative'? combox: new Ext.form.NumberField()
																});							
															} 
															
															cm.setConfig(curColConfig, true); // set true to keep the orignial editors
															evaMatrixGrid.reconfigure(evaMatrixGrid.getStore(), new Ext.grid.ColumnModel(curColConfig));	
															
														}
														else Ext.Msg.alert('Failed', action.result.message);																						
													},
													// If you don't pass success:true, it will always go here
													failure: function(form, action) {
														Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
													}	
												});	
											}
											else {
												Ext.Msg.alert('Validation Error', 'Please select alternatives/criteria to add!' );
											} 
										}			
									}]
								});
										
								new Ext.Window({
									title: 'Select to add new ...',
									layout: 'fit',
									modal: true,
									plain: false,
									closable: true,
									resizable : false,
									buttonAlign:'right',
									items: matrixUpdateForm
								}).show();
							}
						},{
							text: 'Save Values',
							iconCls: 'save',
							handler: function(){
								// to POST only the dirty records to update the values
								var records = [];
								for (var i= 0;i < evaMatrixStore.data.length;i++) {
									rec = evaMatrixStore.data.items[i];
									if (rec.dirty == true){
										records.push((rec.data));
									}
								}
								
								Ext.Ajax.request({
									url:'matrixInfo.php',
									params: {
										task: 'save',
										ws: 'riskgis_three',
										matrixID: id
									},
									jsonData: Ext.util.JSON.encode(records),
									success: function(response, opts) {				
										var obj = Ext.decode(response.responseText);	
										if (obj.success == true) {
											evaMatrixStore.commitChanges();
											Ext.Msg.alert('Success', obj.message);
										}	
										else Ext.Msg.alert('Failure', obj.message);																
									}
								});
							}
						}]
					});
												
					// show the impact matrix (Alt Vs Criteria)
					var evaMatrixWin = new Ext.Window({
						title: 'Decision Problem: ' + rec.get('nom'),
						width: 650,
						height: 400,					
						layout: 'border',
						plain:true,
						maximizable: true,
						collapsible: true,
						closeable: true,
						buttonAlign:'right',
						items: [new Ext.form.FormPanel({
							labelWidth: 75, // label settings here cascade unless overridden
							itemId: 'evaMatrixEditForm',
							region: 'north',
							frame:true,	
							autoHeight:true,		
							defaults: {
								anchor: '100%'
							},
							items: [{
								fieldLabel: 'Title',
								xtype: 'textfield',
								name: 'matrix_title',
								allowBlank: false,
								value: rec.get('nom')
							},{
								fieldLabel: 'Description',
								xtype: 'textarea',
								name: 'matrix_description',
								allowBlank: true,
								value: rec.get('description')
							}],
							buttons: [{
								text: 'Update',
								icon:  'src/gxp/theme/img/silk/table_save.png',
								handler: function(){
									var form = evaMatrixWin.getComponent('evaMatrixEditForm').getForm();				
									if (form.isValid()) {
										form.submit({
											url: 'matrixInfo.php',
											params: {
												task: 'editInfo',
												ws: 'riskgis_three',
												ID: rec.get('id')
											},
											waitMsg : 'Please wait...',
											success: function(form, action) {
												Ext.Msg.alert('Success', action.result.message);
												grid.getStore().reload({ // refresh the grid view
													callback: function(){
														grid.getView().refresh();
														evaMatrixWin.setTitle('Decision Problem: ' + form.findField('matrix_title').getValue());
													}
												});
											},
											failure: function(form, action) {
												Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
											}
										});
									}
									else {
										Ext.Msg.alert('Validation Error', 'The form is not valid!' );
									}
								}
							}]
						}),evaMatrixGrid],
						tools:[
							{
								id:'help',
								qtip: 'Get Help',
								handler: function(event, toolEl, panel){
									Ext.Msg.show({
										title:'Help Information',
										msg: 'The effects of alternatives in terms of the decision criteria are used as inputs for the evaluation process of alternatives. For this purpose, an <b>evaluation matrix</b> is used to compare the performance of each alternative against each criterion. <br><br>Based on the criteria, such performance values should be ideally maximized (benefits) or minimized (costs), using either a quantitative or a qualitative scale according to the type of criterion. The qualitative scale is used to describe how an alternative performs for a specific criterion which cannot be expressed in quantitative terms. This can include, for example, if the impact on the environment caused by a specific alternative is very high.',
										buttons: Ext.Msg.OK,															   
										animEl: 'elId',
										icon: Ext.MessageBox.INFO
									});		
								}
							}]
						});
												
						evaMatrixWin.show();
				}
			},
			failure : function(r){ 
					// the respective alternatives & criteria records failed to retrive from the database
					var res = Ext.decode(r.responseText, true);
					Ext.Msg.alert('Failure', res.message); 
				}
			});
	},
	
	/** api: method[listEvaMatrices]
     */
	listEvaMatrices: function() {
		var xg = Ext.grid;		
		var matrixInfoStore = new Ext.data.GroupingStore({
				url: 'matrixInfo.php',
				baseParams: {
					task: 'load',
					ws: 'riskgis_three',
					userID: userid,
					userRole: role
				},
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:'nom_utilisateur',
				autoLoad: true,
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'}, 			
						{name : 'id_utilisateur', type : 'int'},
						{name : 'nom_utilisateur', type : 'String'}
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>'
			)
		});
		
		var matrixInfoGrid = new xg.GridPanel({
			store: matrixInfoStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					expander,
					{header: "Id", dataIndex: 'id', hidden: true},
					{header: "Title", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur'},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'theme/app/img/table_edit.png',  
								tooltip: 'Evaluate Alternatives Vs Criteria',
								getClass: function(v, meta, rec) {          
									if ( role != 'admin') {
										if (userid != rec.get('id_utilisateur')) {
											return 'x-hide-display';
										}
									}
								},
								handler: function(grid, rowIndex, colIndex) {
									var rec = grid.getStore().getAt(rowIndex);
									var id = rec.get('id');									
									this.editEvaMatrix(id, rec, grid);
								},
								scope: this
							}							
						]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: true,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,		
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','id_utilisateur'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})],
			bbar: [{
				xtype: 'tbtext',
				id: 'listMatrixTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				handler: function(){
					// delete the record from store, db (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete the selected decision problems?',
						msg: 'Are you sure to delete the selected decision problems? This action is undoable!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = matrixInfoGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete the decision problems you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'matrixInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_three',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										matrixInfoGrid.getStore().reload({ 
												callback: function(){
													matrixInfoGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]			
		});
		
		matrixInfoGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listMatrixTotalRec').setText('Total no. of decision problems: ' + records.length.toString());
			}
		);
		
		var matrixInfoWin = new Ext.Window({
			title: 'Decision Problems information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: matrixInfoGrid,
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: 'The decision-making process can benefit from using <b>Multi-Criteria Evaluation (MCE) </b>methods. These methods consider different alternatives of a problem with the aim of addressing trade-offs between alternatives with inclusion of additional important criteria than the traditional cost-benefit analysis. To compare between different management alternatives, the effect of each alternative should be evaluated against each criterion.' + '<br><br>' + 'In this platform, Compromise Programming (CP) method (<a href="./Simonovic_2010.pdf" target="_blank">Simonovic</a>, 2010) is used to calculate the ranking of alternative options. This method identifies alternatives which are the closest to the ideal solution by means of distance (measures of closeness). It supports the selection of an optimum solution assuming that decision makers seek a solution which is as close as possible to the ideal one.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});		
					}
				}]
		});
		
		matrixInfoWin.show();
	},

	/** api: method[createCriterion]
     */
	createCriterion: function() {
		
		var criteriaAddForm = new Ext.form.FormPanel({
			labelWidth: 75, // label settings here cascade unless overridden
			url:'criteriaInfo.php',
			frame:true,			
			bodyStyle:'padding:5px 5px 0',
			width: 450,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Criterion Information',
				collapsible: true,
				defaultType: 'textfield',
				defaults: {
					anchor: "90%",
					allowBlank: false,
					msgTarget: "side"
				},
				collapsed: false,
				items :[{
						fieldLabel: 'Name',
						name: 'criteria_name',
						emptyText: 'Name of the criterion ...'
					},{
						fieldLabel: 'Description',
						xtype: 'textarea',
						name: 'criteria_description',
						emptyText: 'Description of the criterion ...',
						allowBlank: true
					},{
						fieldLabel: 'Category',
						name: 'category',
						xtype: 'combo',
						mode: "local",
						editable: false,
						emptyText: 'Select the category ...',
						store: new Ext.data.ArrayStore({
								id: 0,
								fields: [
									'myId',  // numeric value is the key
									'displayText'
								],
								data: [['economic', 'Economic'], ['social', 'Social'],['environmental', 'Environmental']]  // data is local
						}),
						valueField: 'myId',
						displayField: 'displayText',
						triggerAction: 'all'
					},{
						fieldLabel: 'Type',
						name: 'type',
						xtype: 'combo',
						mode: "local",
						editable: false,
						emptyText: 'Select the type ...',
						store: new Ext.data.ArrayStore({
								id: 0,
								fields: [
									'myId',  // numeric value is the key
									'displayText'
								],
								data: [['Quantitative', 'Quantitative'], ['Qualitative', 'Qualitative']]  // data is local
						}),
						valueField: 'myId',
						displayField: 'displayText',
						triggerAction: 'all'
					}]
			}],
			buttons: [{
				text: 'Add',
				icon: 'src/gxp/theme/img/silk/add.png',
				handler: function(){
					//add a new record to the table
					var form = criteriaAddForm.getForm();				
					if (form.isValid()) {
						form.submit({
							url: 'criteriaInfo.php',
							params: {
								task: 'add',
								ws: 'riskgis_three',
								userID: userid
							},
							waitMsg: 'Please wait...',
							success: function(form, action) {						
								Ext.Msg.alert('Success', action.result.message);	
								form.reset();
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}	
						});	
					}
					else {
						Ext.Msg.alert('Validation Error', 'The form is not valid!' );
					}					
				}
			},{
				text: 'Reset',
				icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
				handler: function(){
					// reset the form and close the dialog box 
					criteriaAddForm.getForm().reset();
				}
			}]	
		});
		
		var criteriaAddWin = new Ext.Window({
			title: 'Define a new criterion..',
			layout: 'form',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: false,
			closeable: true,
			closeAction:'hide',
			buttonAlign:'right',
			items: [criteriaAddForm],
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: 'The formulation of <b>decision criteria</b> beyond the conventional cost-benefit analysis allows for the evaluation of other important and competitive objectives of the decision problem at hand. Consideration of these many aspects supports the use of multi-criteria evaluation (MCE) tools that can facilitate the evaluation of the variety of consequences in a risk management problem without measuring them only at the monetary scale.' + '<br><br>' + 'Three main categories of criteria can be defined in this platform: economic, social and environmental criteria with qualitative or quantitative indicators, to evaluate and compare differences between alternatives. Selected criteria should highlight the extent to which objectives of the problem are satisfied by the management alternatives.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});						
						}
				}]
		});
		
		criteriaAddWin.show();	
	},

	/** api: method[listCriteria]
     */
	listCriteria: function() {
		var xg = Ext.grid;		
		this.CriteriaInfoStore = new Ext.data.GroupingStore({
				url: 'criteriaInfo.php',
				baseParams: {
					task: 'load',
					ws: 'riskgis_three',
					userID: userid,
					userRole: role
				},
				autoLoad: true,
				sortInfo:{field: 'id', direction: "ASC"},
				groupField:'categorie',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'}, 
						{name : 'categorie', type : 'String'}, 
						{name : 'type', type : 'String'},
						{name : 'id_utilisateur', type : 'String'},
						{name : 'nom_utilisateur', type : 'String'}
					] 
				})
		});
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Description:</b> {description}</p>'
			)
		});
		
		var criteriaInfoGrid = new xg.GridPanel({
			store: this.CriteriaInfoStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					expander,
					{header: "Id", dataIndex: 'id', hidden: true},
					{header: "Name", dataIndex: 'nom'},
					{header: "Description", dataIndex: 'description', hidden: true},
					{header: "Category", dataIndex: 'categorie'},					
					{header: "Type", dataIndex: 'type'},
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur'},
					{
						xtype: 'actioncolumn',
						items: [						
							{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  
								tooltip: 'Edit Information',	
								getClass: function(v, meta, rec) {          
									if ( role != 'admin') {
										if (userid != rec.get('id_utilisateur')) {
											return 'x-hide-display';
										}
									}
								},
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);									
									var criteriaEditWin = new Ext.Window({
										title: 'Edit criteria information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'criteriaEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'Name/Title',
												xtype: 'textfield',
												name: 'name',
												allowBlank: false,
												value: rec.get('nom')
											},{
												fieldLabel: 'Description',
												xtype: 'textarea',
												name: 'description',
												value: rec.get('description')
											},{
												fieldLabel: 'Category',
												name: 'category',
												xtype: 'combo',
												mode: "local",
												editable: false,	
												value: rec.get('categorie'),
												store: new Ext.data.ArrayStore({
														id: 0,
														fields: [
															'myId',  // numeric value is the key
															'displayText'
														],
														data: [['economic', 'Economic'], ['social', 'Social'],['environmental', 'Environmental']]  // data is local
												}),
												valueField: 'myId',
												displayField: 'displayText',
												triggerAction: 'all'
											},{
												fieldLabel: 'Type',
												name: 'type',
												xtype: 'combo',
												mode: "local",
												editable: false,
												value: rec.get('type'),
												store: new Ext.data.ArrayStore({
														id: 0,
														fields: [
															'myId',  // numeric value is the key
															'displayText'
														],
														data: [['Quantitative', 'Quantitative'], ['Qualitative', 'Qualitative']]  // data is local
												}),
												valueField: 'myId',
												displayField: 'displayText',
												triggerAction: 'all'
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = criteriaEditWin.getComponent('criteriaEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'criteriaInfo.php',
															params: {
																task: 'edit',
																ws: 'riskgis_three',																
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																criteriaEditWin.close();
																grid.getStore().reload({ // refresh the grid view
																	callback: function(){
																		grid.getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													criteriaEditWin.close();
												}
											}]
										})											
									});
												
									criteriaEditWin.show();
								}
							}
						]
					}	
				],
				defaults: {
					sortable: true,
					menuDisabled: true,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),						
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','id_utilisateur'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})
			],
			bbar: [{
				xtype: 'tbtext',
				id: 'listCriteriaTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				handler: function(){
					// delete the record from store, db (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete selected criteria?',
						msg: 'Are you sure to delete selected criteria? This action is undoable and would remove selected criteria if being used in a certain decision problem!!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = criteriaInfoGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete criteria you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'criteriaInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_three',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										criteriaInfoGrid.getStore().reload({ 
												callback: function(){
													criteriaInfoGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		criteriaInfoGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listCriteriaTotalRec').setText('Total no. of criteria: ' + records.length.toString());
			}
		);
		
		var criteriaInfoWin = new Ext.Window({
			title: 'Criteria information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: criteriaInfoGrid,
			tools:[
			{
				id:'help',
				qtip: 'Get Help',
				handler: function(event, toolEl, panel){
					Ext.Msg.show({
						title:'Help Information',
						msg: 'The formulation of <b>decision criteria</b> beyond the conventional cost-benefit analysis allows for the evaluation of other important and competitive objectives of the decision problem at hand. Consideration of these many aspects supports the use of multi-criteria evaluation (MCE) tools that can facilitate the evaluation of the variety of consequences in a risk management problem without measuring them only at the monetary scale.' + '<br><br>' + 'Three main categories of criteria can be defined in this platform: economic, social and environmental criteria with qualitative or quantitative indicators, to evaluate and compare differences between alternatives. selected criteria should highlight the extent to which objectives of the problem are satisfied by the management alternatives.',
						buttons: Ext.Msg.OK,															   
						animEl: 'elId',
						icon: Ext.MessageBox.INFO
					});							
				}
			}]
		});
		
		criteriaInfoWin.show();
	},

	/** api: method[createWeight]
     */
	createWeight: function() {
		
		var weightAddForm = new Ext.form.FormPanel({
			labelWidth: 75, // label settings here cascade unless overridden
			url:'weightInfo.php',
			frame:true,			
			bodyStyle:'padding:5px 5px 0',
			width: 450,	
			autoHeight:true,		
			items: [{
				xtype:'fieldset',
				title: 'Weight Set Information',
				collapsible: true,
				defaultType: 'textfield',
				defaults: {
					anchor: "90%",
					allowBlank: false,
					msgTarget: "side"
				},
				collapsed: false,
				items :[{
						fieldLabel: 'User Name',
						name: 'user_id',
						hiddenName: 'user_id',
						xtype: 'combo',
						mode: "local",
						editable: false,
						emptyText: 'Select a user to assign..',
						store: new Ext.data.JsonStore({ 
							url: 'userInfo.php',							
							autoLoad: true,
							baseParams: {
								task: 'selectedLoad',
								ws: 'riskgis_three',
								userID: userid,
								userRole: role
							},
							root: 'rows',
							fields : ['user_id','user_name']
						}),
						valueField: 'user_id',
						displayField: 'user_name',
						triggerAction: 'all'
					},{
						fieldLabel: 'Problem',
						name: 'matrix_id',
						hiddenName: 'matrix_id',
						xtype: 'combo',
						mode: "local",
						editable: false,
						emptyText: 'Select a decision problem to assign the user..',
						store: new Ext.data.JsonStore({ 
							url: 'matrixInfo.php',							
							autoLoad: true,
							baseParams: {
								task: 'load',
								ws: 'riskgis_three',
								userID: userid,
								userRole: role
							},
							root: 'rows',
							fields : ['id','nom']
						}),
						valueField: 'id',
						displayField: 'nom',
						triggerAction: 'all'
					},{
						fieldLabel: 'End Date',
						xtype: 'datefield',
						name: 'enddt',
						allowBlank: true,
						emptyText: 'Select the deadline of the prioritization period..',
						minValue: new Date()
					},{
						fieldLabel: 'Remarks',
						xtype: 'textarea',
						name: 'remarks',
						allowBlank: true
					}]
			}],
			buttons: [{
				text: 'Create',
				icon: 'src/gxp/theme/img/silk/add.png',
				handler: function(){
					//save the new weight set record to the table
					var form = weightAddForm.getForm();				
					if (form.isValid()) {
						form.submit({
							url: 'weightInfo.php',
							params: {
								task: 'add',
								ws: 'riskgis_three',
								userID: userid
							},
							waitMsg : 'Please wait...',
							success: function(form, action) {
								if (action.result.success == true) {
									Ext.Msg.alert('Success', action.result.message);	
									form.reset();
								}
								else Ext.Msg.alert('Failed', action.result.message);	
							},
							// If you don't pass success:true, it will always go here
							failure: function(form, action) {
								Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
							}	
						});	
					}
					else {
						Ext.Msg.alert('Validation Error', 'The form is not valid!' );
					}
				}
			},{
				text: 'Reset',
				icon:  'src/gxp/theme/img/silk/arrow_refresh.png', 
				handler: function(){
					// reset the form and close the dialog box 
					weightAddForm.getForm().reset();
				}
			}]	
		});
		
		var weightAddWin = new Ext.Window({
			title: 'Add a new weight set ..',
			layout: 'form',
			autoHeight: true,
			plain:false,
			modal: true,
			resizable: false,
			closeable: true,
			buttonAlign:'right',
			items: [weightAddForm],
			tools:[
					{
						id:'help',
						qtip: 'Get Help',
						handler: function(event, toolEl, panel){
							Ext.Msg.show({
								title:'Help Information',
								msg: 'In the decision-making process, it is important to engage experts, decision makers and the community in order to achieve a sustainable and appropriate risk management. <b>Collaborative decision-making</b> attempts to bring together all concerned parties across and within various horizontal and vertical levels. Encouraging collaboration helps establish individual and community ownership, legitimization of implemented policies and measures, and continued commitment and involvement in risk management efforts. An additional benefit is that collaboration provides an opportunity to enhance interactions between the involved stakeholders through improved cooperation and coordination for risk management activities.<br><br> Mitigation measures derived from a collaborative effort can assist in the creation of a wide range of appropriate, acceptable, cost-effective, and sustainable risk management solutions that respect the characteristics, needs and priorities of a certain risk prone location and its inhabitants.',
								buttons: Ext.Msg.OK,															   
								animEl: 'elId',
								icon: Ext.MessageBox.INFO
							});						
						}
				}]
		});
		
		weightAddWin.show();
	},
	
	/** api: method[listWeight]
     */
	listWeight: function() {
		var xg = Ext.grid;		
		var weightInfoStore = new Ext.data.GroupingStore({
				url: 'weightInfo.php',
				baseParams: {
					task: 'load',
					ws: 'riskgis_three',
					userID: userid,
					userRole: role
				},
				autoLoad: true,
				sortInfo:{field: 'nom_utilisateur', direction: "ASC"},
				groupField:'nom',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'id', type : 'int'},
						{name : 'matrice_id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'id_utilisateur', type : 'int'},
						{name : 'nom_utilisateur', type : 'String'},
						{name : 'delai', type : 'date'},
						{name : 'remarques', type : 'String'},
						{name : 'indice', type : 'String'},
						{name : 'email_add', type : 'String'}						
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Problem description:</b> {description}</p>',
				'<p><b>Remarks:</b> {remarques}</p>'
			)
		});
		
		var weightInfoGrid = new xg.GridPanel({
			store: weightInfoStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					expander,
					{header: "Id", dataIndex: 'id', hidden: true},
					{header: "Matrix ID", dataIndex: 'matrice_id',hidden: true},
					{header: "Decision Problem", dataIndex: 'nom'},
					{header: "Problem Description", dataIndex: 'description', hidden: true},
					{header: "User ID", dataIndex: 'id_utilisateur', hidden: true},
					{header: "User Name", dataIndex: 'nom_utilisateur'},
					{header: "Deadline", dataIndex: 'delai', xtype: 'datecolumn'},
					{header: "Remarks", dataIndex: 'remarques', hidden: true},
					{header: "Mapping Index", dataIndex: 'indice', hidden: true},
					{header: "Mail Address", dataIndex: 'email_add', hidden: true},
					{
						xtype: 'actioncolumn',
						items: [
							{
								icon: 'theme/app/img/rosette.png',  
								tooltip: 'Weight/Visualize',
								getClass: function(v, meta, rec) {          
									if(usrname != rec.get('nom_utilisateur') && role != 'admin') {                                                                      
										return 'x-hide-display';
									}
								}, 
								handler: function(grid, rowIndex, colIndex) {
									var rec = weightInfoGrid.getStore().getAt(rowIndex);									
									this.editWeight(rec);
								},
								scope: this
							},{
								icon: 'src/gxp/theme/img/silk/table_edit.png',  
								tooltip: 'Edit Information',	
								getClass: function(v, meta, rec) {          
									if ( role == 'collective') {
										return 'x-hide-display';										
									}
								},
								handler: function(grid, rowIndex, colIndex) {									
									var rec = grid.getStore().getAt(rowIndex);									
									var weightEditWin = new Ext.Window({
										title: 'Edit weight set information..',
										layout: 'form',
										autoHeight: true,
										plain:false,
										modal: true,
										resizable: false,
										closeable: true,
										closeAction:'hide',
										buttonAlign:'right',
										items: new Ext.form.FormPanel({
											labelWidth: 100, // label settings here cascade unless overridden
											itemId: 'weightEditForm',
											frame:true,			
											bodyStyle:'padding:5px 5px 0',
											width: 350,	
											autoHeight:true,		
											defaults: {
												anchor: '100%'
											},
											items: [{
												fieldLabel: 'User Name',
												name: 'user_name',
												hiddenName: 'user_id',
												xtype: 'combo',
												mode: "local",
												editable: false,
												hiddenValue: rec.get('id_utilisateur'),
												value: rec.get('nom_utilisateur'),
												emptyText: 'Select a user to assign..',
												store: new Ext.data.JsonStore({ 
													url: 'userInfo.php',							
													autoLoad: true,
													baseParams: {
														task: 'selectedLoad',
														ws: 'riskgis_three',
														userID: userid,
														userRole: role
													},
													root: 'rows',
													fields : ['user_id','user_name']
												}),												
												valueField: 'user_id',
												displayField: 'user_name',
												triggerAction: 'all'
											},{
												fieldLabel: 'End Date',
												xtype: 'datefield',
												name: 'enddt',
												allowBlank: true,
												value: rec.get('delai')
											},{
												fieldLabel: 'Remarks',
												xtype: 'textarea',
												name: 'remarks',
												value: rec.get('remarques')
											}],
											buttons: [{
												text: 'Update',
												icon:  'src/gxp/theme/img/silk/table_save.png',
												handler: function(){
													var form = weightEditWin.getComponent('weightEditForm').getForm();				
													if (form.isValid()) {
														form.submit({
															url: 'weightInfo.php',
															params: {
																task: 'editInfo',
																ws: 'riskgis_three',																
																ID: rec.get('id')
															},
															waitMsg : 'Please wait...',
															success: function(form, action) {
																Ext.Msg.alert('Success', action.result.message);
																weightEditWin.close();
																grid.getStore().reload({ // refresh the grid view
																	callback: function(){
																		grid.getView().refresh();
																	}
																});
															},
															failure: function(form, action) {
																Ext.Msg.alert('Failed', action.result ? action.result.message : 'No response');
															}
														});
													}
													else {
														Ext.Msg.alert('Validation Error', 'The form is not valid!' );
													}
												}
											},{
												text: 'Cancel',
												icon:  'src/gxp/theme/img/decline.png',
												handler: function(){
													weightEditWin.close();
												}
											}]
										})											
									});
												
									weightEditWin.show();
								}
							}							
						]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: true,
					width: 5
				}
			}),			
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),	
			frame:true,
			stripeRows: true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['id','id_utilisateur','matrice_id','delai','indice','email_add'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})
			],
			bbar: [{
				xtype: 'tbtext',
				id: 'listWeightTotalRec',
				text: 'Loading ...'
			},'->',{
				text: 'Delete',
				iconCls: 'delete',
				hidden: ( role == 'collective')? true : false,										
				handler: function(){
					// delete the record from store, db (if not admin, user can only delete the ones he/she created)
					Ext.Msg.show({
						title:'Delete selected weight sets?',
						msg: 'Are you sure to delete selected weight sets? This action is undoable and This action is undoable and would remove respective ranking results if any existed!!!',
						buttons: Ext.Msg.YESNOCANCEL,
						fn: function(buttonId, text, opt){
							if (buttonId == 'yes'){
								var records = weightInfoGrid.getSelectionModel().getSelections();
								var ids = [];
								for (var i=0;i<records.length;i++) {
									if (records[i].get('id_utilisateur') == userid || role == 'admin'){ // if not admin, user can only delete the ones he/she created
										ids.push(records[i].data);
									}
									else {										
										Ext.Msg.alert('Information', 'You can only delete weight sets you created, please exclude others from the list of selected records and try again!');
										return;
									}
								}
								
								Ext.Ajax.request({   
									url: 'weightInfo.php',
									params: {
										task: 'delete',
										ws: 'riskgis_three',
										IDs: Ext.encode(ids)
									},
									success: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Information', obj.message);	
										// reload the store and refresh
										weightInfoGrid.getStore().reload({ 
												callback: function(){
													weightInfoGrid.getView().refresh();
												}
											});
									},
									failure: function(response, opts){
										var obj = Ext.decode(response.responseText);										
										Ext.Msg.alert('Failed', obj.message);
									}
								});
							}
						}
					});
				}
			}]
		});
		
		weightInfoGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listWeightTotalRec').setText('Total no. of weight sets: ' + records.length.toString());
			}
		);
		
		var weightInfoWin = new Ext.Window({
			title: 'Weight Sets information',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: weightInfoGrid,
			tools:[
			{
				id:'help',
				qtip: 'Get Help',
				handler: function(event, toolEl, panel){
					Ext.Msg.show({
						title:'Help Information',
						msg: 'In the decision-making process, it is important to engage experts, decision makers and the community in order to achieve a sustainable and appropriate risk management. <b>Collaborative decision-making</b> attempts to bring together all concerned parties across and within various horizontal and vertical levels. Encouraging collaboration helps establish individual and community ownership, legitimization of implemented policies and measures, and continued commitment and involvement in risk management efforts. An additional benefit is that collaboration provides an opportunity to enhance interactions between the involved stakeholders through improved cooperation and coordination for risk management activities.<br><br> Mitigation measures derived from a collaborative effort can assist in the creation of a wide range of appropriate, acceptable, cost-effective, and sustainable risk management solutions that respect the characteristics, needs and priorities of a certain risk prone location and its inhabitants.',
						buttons: Ext.Msg.OK,															   
						animEl: 'elId',
						icon: Ext.MessageBox.INFO
					});							
				}
			},{
				id:'search',
				qtip: 'Play Video',
				handler: function(event, toolEl, panel){
					vid = new Ext.Window({
						title: 'Video Tutorial: selection of alternative (Multi-Criteria Analysis)',
						resizable: false,
						html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/aKSepBhQrVU?rel=0" frameborder="0" allowfullscreen></iframe>'
					});
								
					vid.show();
				}
			}]
		});
		
		weightInfoWin.show();
	},
	
	/** api: method[editWeight]
     */
	editWeight: function(rec) {
		var wColName = rec.get('indice');
		var matrixID = rec.get('matrice_id');
		var weightID = rec.get('id');
		var deadline = rec.get('delai');
		var username = rec.get('nom_utilisateur');
		
		if (deadline == null) { var editable = true;}
		else {
			if (deadline.setHours(0,0,0,0) >= new Date().setHours(0,0,0,0)) var editable = true;
			else var editable = false;
		}
		
		var weightStore = new Ext.data.JsonStore({
			url: 'weightInfo.php',
			baseParams: {
				task: 'edit',
				ws: 'riskgis_three',
				weightCol: wColName,
				matrixID: matrixID
			},
			root: 'rows',
			fields: ['id','nom', 'description', {name: wColName, type: 'int'}],
			autoLoad: true
		});
		
		var weightGrid = new Ext.grid.EditorGridPanel({
			store: weightStore,
			colModel: new Ext.grid.ColumnModel({
				defaults: {
					width: 120,
					sortable: true
				},
				columns: [
					{header: "ID", width: 50, sortable: true, dataIndex: 'id', hidden: true},
					{header: "Name", width: 50, sortable: true, dataIndex: 'nom'},
					{header: "Description", width: 100, sortable: true, dataIndex: 'description'},
					{header: "Weight", width: 40, sortable: true, dataIndex: wColName, editable: editable, editor: new Ext.form.NumberField({allowNegative:false})},
					{header: "Normalized Weight", width: 40, renderer: function(v, params, record, rowIndex, colIndex, store){	
						return (record.get(wColName)/store.sum(wColName)).toFixed(4); 	
						}													
					}
				]
			}),
			viewConfig: {
				forceFit: true
			},
			listeners: {
				afteredit: function(e){
					e.grid.getView().refresh(); // to re-render the grid after each edit of the rows
				}
			},
			sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
			width: 400,
			height: 300,									
			frame: true,
			tbar:[],
			buttons: [{
				text: 'Show Rank', // to calculate the ranking based on the given weight set and save the results in table
				icon: 'theme/app/img/rosette.png',
				handler: function(){
					
					var wrecords = []; 
					for (var i= 0;i < weightStore.data.length;i++) {						
						wrecords.push((weightStore.data.items[i].data)); // *retrieve the current weight records from weightStore*																
					}
					
					var rStore = new Ext.data.JsonStore({						
						url: 'rankInfo.php',
						root: 'rows',
						fields: ['alternative_id','alternative_name','distance','ranking']
					});
													
					var wStore = new Ext.data.JsonStore({
						fields: ['nom', wColName],
						data: wrecords
					});
					
					var RankGrid = new Ext.grid.GridPanel({
						store: rStore,
						colModel: new Ext.grid.ColumnModel({
							columns: [
							{header: 'ID', width: 5, dataIndex: 'alternative_id', hidden: 'true'},
							{header: 'Alternatives', width: 40, sortable: true, dataIndex: 'alternative_name'},
							{header: 'Distance Values', width: 20, sortable: true, dataIndex: 'distance'},
							{header: 'Ranking', width: 20, sortable: true, dataIndex: 'ranking',
								renderer: function(value, meta) {
									if (parseInt(value) == 1) { 
										meta.style = "background-color:#FFFF00;";
										return value;
									}
									else return value;
								}
							}
							]
						}),
						viewConfig: {
							forceFit: true
						},
						sm: new Ext.grid.RowSelectionModel({singleSelect:true}),
						width: 400,
						height: 400,													
						frame: true,
						title: 'Compromise Programming Approach',
						tools:[{
							id:'help',
							qtip: 'Get Help',
								handler: function(event, toolEl, panel){
									Ext.Msg.show({
										title:'Help Information',
										msg: 'The ranking of alternatives were calculated using Compromise Programming approach. This method identifies alternatives which are the closest to the ideal solution by means of distance (measures of closeness). The alternative with the minimum distance value is ranked first and considered as the "best compromise solution".' + "<br><br>" + 'The left bar chart shows the ranking of alternatives and the right pie chart represents the given weights (importance of criteria).',
										buttons: Ext.Msg.OK,															   
										animEl: 'elId',
										icon: Ext.MessageBox.INFO
									});
								}
						}],
						tbar:[],
						buttons: [{
							text: 'Save Results and Weights',
							iconCls: 'save',
							handler: function(){
								// save the ranking distances back to the table in db 
								// override the results if already existed
								Ext.Msg.show({
									title:'Override Existing Records?',
									msg: 'If there are any existing results of this weight set, it will be overridden. Do you want to continue?',
									buttons: Ext.Msg.YESNOCANCEL,
									fn: function(btn){
									if (btn == "yes"){
										//YES 
										Ext.Ajax.request({
											url:'weightInfo.php',
											params: {
												task: 'saveweight',
												ws: 'riskgis_three',
												weightCol: wColName
											},
											jsonData: Ext.util.JSON.encode(wrecords),
											success: function(response, opts) {				
												// if weights have been saved successfully, save ranking results to the db
												var records = [];
												for (var i= 0;i < rStore.data.length;i++) {
													rec = rStore.data.items[i];
													records.push((rec.data));																	
												}
																					
												Ext.Ajax.request({
													url:'rankInfo.php',
													params: {
														task: 'save',
														ws: 'riskgis_three',
														matrixID: matrixID,
														weightID: weightID
													},
													jsonData: Ext.util.JSON.encode(records),
													success: function(response, opts) {
														var obj = Ext.decode(response.responseText);										
														Ext.Msg.alert('Success', obj.message);						
													}
												});					
											}
										});
																	
										}			
									},
									animEl: 'elId',
									icon: Ext.MessageBox.QUESTION
								});
							}
						}]	
					});
					
					tab = new Ext.Panel({
						title: 'Ranking Information',	
						layout: 'vbox',	
						layoutConfig: {
							align: 'stretch',
							pack: 'start'
						},
						closable: true,													
						items: [{														
							flex: 1,
							layout: 'fit',
							border: false,
							items: [RankGrid]
						},{
							flex: 1,
							layout: 'hbox',
							layoutConfig: {
								align: 'stretch',
								pack: 'start'
							},
							items: [{ 
								flex: 1,
								layout: 'fit',
								border: false,
								items: new Ext.Panel({															
									width: 400,
									height: 400,
									title: 'Bar Chart: Ranking of Alternatives',															
									items: {
										xtype: 'columnchart',
										store: rStore,
										yField: 'ranking',
										url: 'src/ext/resources/charts.swf',
										xField: 'alternative_name',
										xAxis: new Ext.chart.CategoryAxis({
											title: 'Alternatives'
										}),
										yAxis: new Ext.chart.NumericAxis({
											title: 'Rank',
											minorUnit: 1
										}),
										extraStyle: {
											xAxis: {
												labelRotation: -90
											}
										}
									}		
								})
							},{															
								flex: 1,
								layout: 'fit',
								border: false,
								items: new Ext.Panel({
									width: 400,
									height: 400,																
									title: 'Pie Chart: Given weights (in percentage) for each criteria',
									items: {
										xtype: 'piechart',
										store: wStore,
										dataField: 'w_'+ weightID,
										categoryField: 'nom',
										url: 'src/ext/resources/charts.swf',													
										extraStyle: {															
											legend: {
												display: 'bottom',
												padding: 5,
												font: {
													family: 'Tahoma',
													size: 13
												}
											}
										}	
									}
								})
							}]
						}]
					});
												
					RankGrid.getStore().reload({ // reload the alternative store with the selected workspace 
						params: 
							{	ws: 'riskgis_three',														
								weightCol: wColName,
								matrixID: matrixID,
								wrecords:Ext.encode(wrecords),
								task: 'calculateDyn'
							} 
					});				
					
					weightTabs.add(tab).show;
					
					/* //Create the Download button and add it to the top toolbar
					var exportButton = new Ext.ux.Exporter.Button({
						component: RankGrid,
						text     : "Download as .xls"
					});
												
					RankGrid.getTopToolbar().add(exportButton); */
				}
			}]
		});	
		
		/* //Create the Download button and add it to the top toolbar
		var exportButton = new Ext.ux.Exporter.Button({
			component: weightGrid,
			text     : "Download as .xls"
		});
									
		weightGrid.getTopToolbar().add(exportButton); */
		
		var weightTabs = new Ext.TabPanel({		
			activeTab: 0,
			items: [{
				title: 'Weighting of criteria',
				layout: 'fit',
				items: [weightGrid]
			},{
				title: 'Evaluation of Alternatives Vs Criteria',
				layout: 'fit',	
				itemId: 'weightEvaGrid',
				items: [],
				listeners : {
					beforerender: {
						fn : function() {
							Ext.Ajax.request({
								url: 'matrixInfo.php',
								params : {
									task: 'edit',
									ws: 'riskgis_three',
									matrixID: matrixID
								},
								success : function(r){
									//create a json object from the response string																
									var res = Ext.decode(r.responseText, true);
									// if we have a valid json object, then process it
									if(res !== null &&  typeof (res) !==  'undefined'){
										var weightEvaMatrixStore = new Ext.data.JsonStore({
											url: 'matrixInfo.php',
											root: 'rows',
											fields: res.data,
											autoLoad: true,
											baseParams: {
												task: 'edit',
												ws: 'riskgis_three',
												matrixID: matrixID
											}
										});
										//	Retrive the dynamic column names from the php response
										var cols = [];
										cols[0] = {header: "ID", dataIndex: "alternative_id", width:5, hidden: true}; // alternative_id
										cols[1] = {header: "Alternatives Vs Criteria", dataIndex: "alternative_name", width:150}; // alternative_name
										for (var i = 2; i < res.data.length; i++)
										{
											cols[i] = {header: res.data[i], dataIndex: res.data[i], 
											renderer: res.type[i] == 'Qualitative'? function(value, meta) {
												if (parseInt(value) == 1){
													return "Extremely Bad";		
												} else if (parseInt(value) == 2){
													return "Very Bad";	
												} else if (parseInt(value) == 3){
													return "Bad";	
												} else if (parseInt(value) == 4){
													return "More or Less Bad";	
												} else if (parseInt(value) == 5){
													return "Moderate";	
												} else if (parseInt(value) == 6){
													return "More or Less Good";	
												} else if (parseInt(value) == 7){
													return "Good";	
												} else if (parseInt(value) == 8){
													return "Very Good";	
												} else if (parseInt(value) == 9){
													return "Perfect";	
												} else {
													return value;
												}
											}: function(value, meta) {
												return value;
											}
																		
											};
										}			
																						
										var weightEvaMatrixGrid = new Ext.grid.GridPanel({
											store: weightEvaMatrixStore,
											colModel: new Ext.grid.ColumnModel({
												defaults: {
													sortable: true,
													width: 100
												},
												columns: cols														
											}),
											viewConfig: {														
												forceFit: true									
											},
											sm: new Ext.grid.RowSelectionModel({singleSelect:true}),												
											height: 300,													
											frame: true,
											stripeRows: true,
											split: true
										});
										//	debugger;
										
										weightTabs.getComponent('weightEvaGrid').add(weightEvaMatrixGrid);
										weightTabs.getComponent('weightEvaGrid').doLayout(); // *important*
									}
								}
							});	
						}
					}
				}
			}]
		});	
										
		var weightCriteriaWin = new Ext.Window({
			title: 'Priortization of criteria for the decision problem: '+ rec.get('nom') + ' by the user: ' + rec.get('nom_utilisateur'),
			width: 500,
			height: 400,
			layout: 'fit',
			plain: true,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: [weightTabs],
			tools:[
				{
					id:'help',
					qtip: 'Get Help',
					handler: function(event, toolEl, panel){
						Ext.Msg.show({
							title:'Help Information',
							msg: 'A simple numeric scale of 1 to 5 is used to indicate the preferences of stakeholders on the criteria. Such a choice of weighting scale was implemented in the platform to simplify the complexity in determining preferences. Weights are then normalized (in which weight values are divided by the total weights) to be used for ranking of alternatives using CP method. The ranking of alternatives is based on weighted aggregation method to combine the performance values of alternatives into one overall measure. Thereby, defined criteria and their evaluation of values against each alternative are aggregated based on the weighting preferences of the decision makers, producing the ranking of preferred alternatives which are recommended for implementation. <br><br>To learn more about how the ranking of alternatives is calculated using the CP method, read the following <a href="./Simonovic_2010.pdf" target="_blank">document</a> of Simonovic (Eq. 7.9, p. 270).',
							buttons: Ext.Msg.OK,															   
							animEl: 'elId',
							icon: Ext.MessageBox.INFO
						});							
					}
				},{
					id:'search',
					qtip: 'Play Video',
					handler: function(event, toolEl, panel){
						vid = new Ext.Window({
							title: 'Video Tutorial: selection of alternative (Multi-Criteria Analysis)',
							resizable: false,
							html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/aKSepBhQrVU?rel=0&amp;start=66"" frameborder="0" allowfullscreen></iframe>'
						});
									
						vid.show();
					}
				}]
			});
									
		weightCriteriaWin.show();
	},
	
	/** api: method[listGroupRank]
     */
	listGroupRank: function() {
		var xg = Ext.grid;		
		var rankInfoStore = new Ext.data.GroupingStore({
				url: 'rankInfo.php',
				baseParams:{
					ws: 'riskgis_three',														
					task: 'load',
					userID: userid,
					userRole: role
				},
				autoLoad: true,
				sortInfo:{field: 'nom', direction: "ASC"},
				groupField:'nom',
				reader: new Ext.data.JsonReader({
					totalProperty : 'totalCount',
					root          : 'rows',
					successProperty: 'success',
					idProperty    : 'id',
					fields: [					
						{name : 'matrice_id', type : 'int'},
						{name : 'nom', type : 'String'},
						{name : 'description', type : 'String'},
						{name : 'poids_id', type : 'int'},						
						{name : 'nom_utilisateur', type : 'String'}
					] 
				})
		});	
		
		var expander = new Ext.grid.RowExpander({
			tpl : new Ext.Template(
				'<p><b>Problem description:</b> {description}</p>'
			)
		});

		var rankInfoGrid = new xg.GridPanel({
			store: rankInfoStore,
			colModel: new Ext.grid.ColumnModel({
				columns: [
					expander,
					{header: "Matrix Id", width: 5, dataIndex: 'matrice_id', hidden: true},
					{header: "Wieght Id",width: 5, dataIndex: 'poids_id', hidden: true},
					{header: "Name of the decision problem", width: 30, dataIndex: 'nom'},
					{header: "Description of the problem", width: 50, dataIndex: 'description', hidden: true},					
					{header: "Weighted User Name", width: 20, dataIndex: 'nom_utilisateur'},
					{
					xtype: 'actioncolumn',					
						items: [	
							{
								icon: 'theme/app/img/chart_bar.png',  
								tooltip: 'Visualize',
								handler: function(grid, rowIndex, colIndex) {
									var rec = rankInfoGrid.getStore().getAt(rowIndex);
									var weightID = rec.get('poids_id'); 
									var matrixID = rec.get('matrice_id');
								
									var rankInfoVDStore = new Ext.data.JsonStore({
										url: 'rankInfo.php',
										root: 'rows',
										autoLoad: true,
										baseParams: 
										{	ws: 'riskgis_three',														
											weightID: weightID,
											matrixID: matrixID,
											task: 'queryBarDistance'
										}, 
										fields: ['alternative_name','distance','rank']
									});
									
									var rankInfoVWStore = new Ext.data.JsonStore({
										url: 'rankInfo.php',
										root: 'rows',
										autoLoad: true,
										baseParams: 
										{	ws: 'riskgis_three',														
											weightID: weightID,
											matrixID: matrixID,
											task: 'queryBarWeight'
										}, 
										fields: ['criteria_name','w_'+ weightID]
									});
													
									tab = new Ext.Panel({										
										layout: 'hbox',		
										layoutConfig: {
											align: 'stretch',
											pack: 'start'
										},
										border: false,										
										items: [{
											flex: 1,
											layout: 'fit',
											items: new Ext.Panel({															
												width: 400,
												height: 400,			
												border: false,
												title: 'Column Chart: Ranking of Alternatives',															
												items: [{
													xtype: 'columnchart',
													store: rankInfoVDStore,
													url: 'src/ext/resources/charts.swf',
													xField: 'alternative_name',													
													yAxis: new Ext.chart.NumericAxis({
														title: 'Rank',
														majorUnit: 1
													}),	
													series: [{
														type: 'column',
														displayName: 'Ranking:',
														yField: 'rank',
														style: {															
															mode: 'stretch'/* ,
															color: 0x99BBE8 */
														}													
													}],
													extraStyle: {
														xAxis: {
															labelRotation: -90
															}
														}
													}]		
												})
											},{															
												flex: 1,
												layout: 'fit',
												items: new Ext.Panel({
													width: 400,
													height: 400,	
													border: false,
													title: 'Pie Chart: Weight of Criteria (in percentage)',
													items: [{
														xtype: 'piechart',
														store: rankInfoVWStore,
														dataField: 'w_'+ weightID,
														categoryField: 'criteria_name',
														url: 'src/ext/resources/charts.swf',													
														extraStyle: {															
															legend: {
																display: 'bottom',
																padding: 5,
																font: {
																	family: 'Tahoma',
																	size: 13
																}
															}
														}
													}]
												})
											}]
										});
										
									var rankInfoVisoWin = new Ext.Window({
										title: 'Ranking Information of the user: ' + rec.get('nom_utilisateur') + ' for the decision problem: ' + rec.get('nom'),
										width: 550,
										height: 350,
										minWidth: 300,
										minHeight: 200,
										layout: 'fit',
										plain: true,
										maximizable: true,
									//	collapsible: true,
										closeable: true,
										buttonAlign:'right',
										items: [tab],
										tools:[
											{
												id:'help',
												qtip: 'Get Help',
												handler: function(event, toolEl, panel){
													Ext.Msg.show({
														title:'Help Information',
														msg: 'The left bar chart shows the ranking of alternatives, meaning the alternative with value "1" is ranked as "FIRST".'+ "<br><br>" + 'The right pie chart represents the given weights (importance of criteria).',
														buttons: Ext.Msg.OK,															   
														animEl: 'elId',
														icon: Ext.MessageBox.INFO
													});
											}
										}]
									});
									
									rankInfoVisoWin.show();
								},
								getClass: function(v, meta, rec) {                              
                                    return 'x-action-col-icon';
                                }
							},{
								icon: 'theme/app/img/group.png',  
								tooltip: 'Group Comparison',
								getClass: function(v, meta, rec) {          
									if ( role == '') {
										return 'x-hide-display';										
									}
								},
								handler: function(grid, rowIndex, colIndex) {
									var rec = rankInfoGrid.getStore().getAt(rowIndex);
									var matrixID = rec.get('matrice_id');
									var series = series_weight = [];
									
									Ext.Ajax.request({
										url : 'queryColNames.php',
										params : {
											ws: 'riskgis_three',
											matrixID: matrixID
										},
										success : function(r){
											//create a json object from the response string
											var res = Ext.decode(r.responseText, true);
											// if we have a valid json object, then process it
											if(res !== null &&  typeof (res) !==  'undefined'){
											
												// create the group distance store #res.data
												var rankInfoVGDStore = new Ext.data.JsonStore({
													url: 'rankInfo.php',
													root: 'rows',
													autoLoad: true,
													baseParams: 
													{	ws: 'riskgis_three',
														matrixID: matrixID,
														task: 'queryStackedBarDistance'
													},
													fields: res.data
												});
																								
												//	create the series array for the group distance stacked bar chart 												
												for (var i = 0; i < res.data.length-2; i++){										
													series[i] = { xField: res.data[i], displayName: res.data[i]};
												}
												
												tabGroup = new Ext.Panel({										
													layout: 'hbox',		
													layoutConfig: {
														align: 'stretch',
														pack: 'start'
													},
													border: false,													
													items: [{
														flex: 1,
														layout: 'fit',
														items: new Ext.Panel({
															width: 400,
															height: 400,																
															title: 'Stacked Bar Chart: Group comparison of rankings by different users' + '<br>' + '(the shortest bar color represents the best compromise alternative)',
															items: {
																xtype: 'stackedbarchart',
																store: rankInfoVGDStore,
																url: 'src/ext/resources/charts.swf',
																yField: 'user_name',
																xAxis: new Ext.chart.NumericAxis({
																	stackingEnabled: true
																}),
																series: series,
																extraStyle: {															
																	legend: {																		
																		display: 'bottom',
																		padding: 15,
																		font: {
																			family: 'Tahoma',
																			size: 13
																		}
																	}
																}
															}
														})
													}												
													]
												});
												
												// create the window to show
												var rankInfoGroupWin = new Ext.Window({
													title: 'Group ranking information of the decision problem: ' + rec.get('nom'),
													width: 550,
													height: 350,
													minWidth: 300,
													minHeight: 200,
													layout: 'fit',
													plain: true,
													maximizable: true,
													closeable: true,
													closeAction:'hide',
													buttonAlign:'right',
													items: [tabGroup],
													tools:[
														{
															id:'help',
															qtip: 'Get Help',
															handler: function(event, toolEl, panel){
																Ext.Msg.show({
																	title:'Help Information',
																	msg: 'The stacked bar chart shows the distance values of alternatives for each user (decision maker). The shortest bar color alternative (i.e. minimum distance value) is considered as the "best compromise solution".',
																	buttons: Ext.Msg.OK,															   
																	animEl: 'elId',
																	icon: Ext.MessageBox.INFO
																});
														}
													}]
												});	
												
												rankInfoGroupWin.show();
											}
										},									
										failure : function(r){ 
											// the respective alternatives & criteria records failed to retrive from the database
											var res = Ext.decode(r.responseText, true);
											Ext.Msg.alert('Failure', res.message); 
										}
									});	
								}
							}
						]
					}
				],
				defaults: {
					sortable: true,
					menuDisabled: true,
					width: 20
				}
			}),	
			view: new Ext.grid.GroupingView({				
				forceFit: true,
				groupTextTpl: '{text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			}),			
			stripeRows: true,
			frame:true,
			width: 700,
			height: 450,
			plugins:[expander,
				new Ext.ux.grid.Search({
					iconCls:'gxp-icon-zoom-to',
					disableIndexes:['poids_id','matrice_id'],
					minChars:2,
					autoFocus:true,
					width: 150,
					mode: 'local'
				})
			],
			bbar: [{
				xtype: 'tbtext',
				id: 'listRankTotalRec',
				text: 'Loading ...'
			},'->']
		});	
		
		rankInfoGrid.getStore().on('load', 					
			function (store, records, options) {
				Ext.getCmp('listRankTotalRec').setText('Total no. of ranking results: ' + records.length.toString());
			}
		);
		
		var rankInfoWin = new Ext.Window({
			title: 'Ranking results of the defined decision problems',
			layout: 'fit',		
			plain:false,
			maximizable: true,
			collapsible: true,
			closeable: true,
			buttonAlign:'right',
			items: rankInfoGrid,
			tools:[
			{
				id:'help',
				qtip: 'Get Help',
				handler: function(event, toolEl, panel){
					Ext.Msg.show({
						title:'Help Information',
						msg: 'The ranking of alternatives (distance value or closeness of measure to the ideal solution) is calculated with the given weight set of the user and evaluation matrix using the CP method (Equation 7.9 of <a href="./Simonovic_2010.pdf" target="_blank">Simonovic</a>, 2010). <br><br> In this platform, the comparison of ranking information resulting from other decision maker users can also be visualized for the purpose of negotiation to achieve a final agreeable outcome of alternatives and a visible expression and communication of different preferences.',
						buttons: Ext.Msg.OK,															   
						animEl: 'elId',
						icon: Ext.MessageBox.INFO
					});							
				}
			},{
				id:'search',
				qtip: 'Play Video',
				handler: function(event, toolEl, panel){
					vid = new Ext.Window({
						title: 'Video Tutorial: formulation of alternative (risk reduction) scenario',
						resizable: false,
						html: '<iframe width="560" height="315" src="https://www.youtube.com/embed/aKSepBhQrVU?rel=0&amp;start=176" frameborder="0" allowfullscreen></iframe>'
					});
								
					vid.show();
				}
			}]
		});
		
		rankInfoWin.show();
	}
	
});	

Ext.preg(riskgis.plugins.DecisionAnalysis.prototype.ptype, riskgis.plugins.DecisionAnalysis);
	
	
