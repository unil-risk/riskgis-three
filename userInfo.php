<?php
	require_once 'dbConnect.php'; // Connect to the database
	
	$workspace = $_POST['ws'];
	$task = $_POST['task'];
	$userID = $_POST['userID'];	
	$userRole = $_POST['userRole'];
	
	if (!$dbconn){
		echo "An error occured.\n";
		exit;
	}
	
	/* if ($task == 'selectedLoad') {
		$query = "SELECT user_id_users AS user_id, user_name FROM public.users_ws
			WHERE '$workspace' = ws_name;"; 
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	} */
	
	if ($task == 'selectedLoad') {
		// allow all user names to be visible if logged in with 'admin' role
		if ($userRole == 'admin') {
			$query = "SELECT id AS user_id, user_name FROM $workspace.users;"; 
		}
		else {
			// allow only own his/her username to be visible if logged in with other roles (i.e. as individually)
			$query = "SELECT id AS user_id, user_name 
			FROM $workspace.users
			WHERE id = $userID;"; 
		}
		$arr=array();
		
		If (!$rs = pg_query($dbconn,$query)) {
			Echo '{success:false,message:'.json_encode(pg_last_error($dbconn)).'}';
		}
		else {
				while($obj = pg_fetch_object($rs)){
				$arr[] = $obj;
			}
			Echo '{success:true,rows:'.json_encode($arr).'}';
		}
	}
?>